package ru.sberbank.views.client.deposit;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.deposit.BankingAccount;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.service.dbService.deposit.BankingAccountRepositoryService;
import ru.sberbank.model.service.serverProcess.deposit.BankingAccountServiceImpl;

import java.text.SimpleDateFormat;

@Service
public class ListBankingAccount extends Div {
    private Client client;
    private Grid<BankingAccount> bankingAccountGrid;
    private BankingAccountServiceImpl accountService;
    private BankingAccount selectAccount;
    private BankingAccountRepositoryService bankingAccountRepositoryService;
    private Div messageAccount;

    public ListBankingAccount(@Autowired BankingAccountServiceImpl accountService,
                              @Autowired BankingAccountRepositoryService bankingAccountRepositoryService) {
        this.bankingAccountRepositoryService = bankingAccountRepositoryService;
        this.accountService = accountService;
        setSizeFull();
        messageAccount = new Div();
        bankingAccountGrid = new Grid<>();
        itemsAccount();
        updateAccountGrid();

    }

    private void itemsAccount() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        bankingAccountGrid.addColumn(BankingAccount::getNumberBankAccount).setHeader("Номер счета");
        bankingAccountGrid.addColumn(BankingAccount::getBalance).setHeader("Сумма на счету");
        bankingAccountGrid.addColumn(bankingAccount -> format.format(bankingAccount.getDateEnd())).setHeader("Дата закрытия счета");
        VerticalLayout verticalLayout = new VerticalLayout();

        bankingAccountGrid.getColumns().forEach(column -> column.setAutoWidth(true));
        bankingAccountGrid.recalculateColumnWidths();
        bankingAccountGrid.setSizeUndefined();
        bankingAccountGrid.asSingleSelect().addValueChangeListener(event -> {
            selectAccount = event.getValue();
            if (selectAccount != null) {
                String message = String.format("Информация по кредитной карте: \n %s",
                        selectAccount.toString());
                messageAccount.setText(message);
            }
        });
        verticalLayout.add(bankingAccountGrid, messageAccount, formLayout());

        add(verticalLayout);
    }

    private FormLayout formLayout() {
        FormLayout transactionLayout = new FormLayout();

        BigDecimalField summField = new BigDecimalField();
        summField.setLabel("Введите сумму");

        Button depositButton = new Button("Пополнить");
        Button creditButton = new Button("Снять");
        Button closeButton = new Button("Закрыть");

        try {
            depositButton.addClickListener(click -> {
                accountService.debitBankingAccount(selectAccount, summField.getValue());
                updateAccountGrid();
            });
            creditButton.addClickListener(event -> {
                accountService.creditBankingAccount(selectAccount, summField.getValue());
                updateAccountGrid();
            });
            closeButton.addClickListener(event -> {
                accountService.closeBankingAccount(selectAccount);
                updateAccountGrid();
            });
        } catch (Exception e) {
            Notification.show(e.getMessage());
        }
        transactionLayout.add(summField, depositButton, creditButton, closeButton);
        return transactionLayout;
    }

    public void updateAccountGrid() {
        if (client != null) {
            bankingAccountGrid.setItems(bankingAccountRepositoryService.findAllByClient(client));
            selectAccount = null;
            messageAccount.setText(" ");
        }
    }

    public void setClient(Client client) {
        this.client = client;
        updateAccountGrid();
    }
}
