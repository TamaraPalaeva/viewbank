package ru.sberbank.views.client.deposit;

import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.html.Div;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.person.Client;

@Service
public class DepositClientView extends Div {

    private AddBankingAccount addBankingAccount;
    private ListBankingAccount listBankingAccount;
    private Client client;

    public void setClient(Client client) {
        this.client = client;
        addBankingAccount.setClient(client);
        listBankingAccount.setClient(client);
        listBankingAccount.updateAccountGrid();
        addBankingAccount.updateRateOnDepositGrid();
    }

    public DepositClientView(@Autowired AddBankingAccount addBankingAccount,
                             @Autowired ListBankingAccount listBankingAccount) {
        this.addBankingAccount = addBankingAccount;
        this.listBankingAccount = listBankingAccount;
        setSizeFull();
        Accordion accordion = new Accordion();
        accordion.setWidth("1500px");
        accordion.add("Создать вклад", addBankingAccount);
        accordion.add("Список вкладов", listBankingAccount);
        accordion.setSizeFull();
        accordion.close();
        accordion.addOpenedChangeListener(e -> {
            listBankingAccount.updateAccountGrid();
            addBankingAccount.updateRateOnDepositGrid();

        });
        add(accordion);
    }
}
