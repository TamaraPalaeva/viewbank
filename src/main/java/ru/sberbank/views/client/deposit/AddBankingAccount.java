package ru.sberbank.views.client.deposit;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.deposit.AnswerBankingAccount;
import ru.sberbank.model.entity.deposit.RateOnDeposit;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.service.dbService.deposit.RateOnDepositRepositoryService;
import ru.sberbank.model.service.serverProcess.deposit.BankingAccountServiceImpl;

import java.math.BigDecimal;

@Service
public class AddBankingAccount extends Div {
    private Client client;
    private Grid<RateOnDeposit> rateOnDepositGrid;
    private RateOnDeposit selectRate;
    private BankingAccountServiceImpl bankingAccountService;
    private RateOnDepositRepositoryService rateOnDepositRepositoryService;
    private Div messageDiv;

    public void setClient(Client client) {
        this.client = client;
    }

    public AddBankingAccount(@Autowired BankingAccountServiceImpl bankingAccountService,
                             @Autowired RateOnDepositRepositoryService rateOnDepositRepositoryService) {
        this.bankingAccountService = bankingAccountService;
        this.rateOnDepositRepositoryService = rateOnDepositRepositoryService;
        setSizeFull();
        rateOnDepositGrid = new Grid<>();
        createGrid();
        updateRateOnDepositGrid();
    }

    private void createGrid() {
        messageDiv = new Div();
        updateRateOnDepositGrid();
        rateOnDepositGrid.addColumn(RateOnDeposit::getDurationOfDeposit).setHeader("Срок вклада");
        rateOnDepositGrid.addColumn(rateOnDeposit -> {
            return rateOnDeposit.getMinAmount() + " - " + rateOnDeposit.getMaxAmount();
        }).setHeader("Ограничение по сумме");
        rateOnDepositGrid.addColumn(RateOnDeposit::getPercentDeposit).setHeader("Процент по кредиту");
        rateOnDepositGrid.addColumn(rateOnDeposit -> {
            if (rateOnDeposit.isCapital())
                return "V";
            else return "";
        }).setHeader("Капитализация");
        rateOnDepositGrid.addColumn(rateOnDeposit -> {
            if (rateOnDeposit.isCharge())
                return "V";
            else return "";
        }).setHeader("Возможность снять");
        rateOnDepositGrid.addColumn(rateOnDeposit -> {
            if (rateOnDeposit.isClose())
                return "V";
            else return "";
        }).setHeader("Закрытие досрочно");
        rateOnDepositGrid.addColumn(rateOnDeposit -> {
            if (rateOnDeposit.isRefill())
                return "V";
            else return "";
        }).setHeader("Возможность пополнить счет");

        rateOnDepositGrid.getColumns().forEach(column -> column.setAutoWidth(true));

        rateOnDepositGrid.recalculateColumnWidths();
        rateOnDepositGrid.setHeightFull();
        rateOnDepositGrid.setSizeUndefined();
        rateOnDepositGrid.asSingleSelect().addValueChangeListener(event -> {
            selectRate = event.getValue();
            if (selectRate != null) {
                String message = String.format("Информация по кредитной карте: \n %s",
                        selectRate.toString());
                messageDiv.setText(message);
            }
        });

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(rateOnDepositGrid, messageDiv, formLayout());

        add(verticalLayout);
    }

    public void updateRateOnDepositGrid() {
        if (client != null) {
            rateOnDepositGrid.setItems(rateOnDepositRepositoryService.findAll());
            selectRate = null;
            messageDiv.setText("");
        }
    }

    private FormLayout formLayout() {
        FormLayout transactionLayout = new FormLayout();

        BigDecimalField summField = new BigDecimalField();
        summField.setLabel("Введите сумму");

        Button createButton = new Button("Открыть счет");

        try {
            createButton.addClickListener(click -> {
                bankingAccountService.createBankingAccount(new AnswerBankingAccount(selectRate, client), summField.getValue());
                updateRateOnDepositGrid();
                summField.setValue(new BigDecimal(0));
            });
        } catch (Exception e) {
            Notification.show(e.getMessage());
        }
        transactionLayout.add(summField, createButton);
        return transactionLayout;
    }
}
