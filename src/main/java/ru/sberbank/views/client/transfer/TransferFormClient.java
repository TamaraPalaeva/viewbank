package ru.sberbank.views.client.transfer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;
import ru.sberbank.model.entity.transfer.Transfer;
import ru.sberbank.model.exception.FindAccountException;
import ru.sberbank.model.exception.SummException;
import ru.sberbank.model.service.dbService.service.NumberAccountRepositoryService;
import ru.sberbank.model.service.dbService.transfer.TransferDAOService;
import ru.sberbank.model.service.serverProcess.transfer.TransactionService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TransferFormClient extends VerticalLayout {
    private TransferDAOService transferDAOService;
    private TransactionService transactionService;
    private NumberAccountRepositoryService numberAccountRepositoryService;
    private List<NumberAccount> numberAccountListAll = new ArrayList<>();
    private List<NumberAccount> numberAccountListByClient = new ArrayList<>();
    private Grid<Transfer> historyTransfer;
    private Client client;
    private TextField sender = new TextField();
    private TextField recipient = new TextField();
    private BigDecimalField amount = new BigDecimalField();
    private ComboBox<NumberAccount> senderCombo = new ComboBox<>();
    private ComboBox<NumberAccount> recipientCombo = new ComboBox<>();

    public void setClient(Client client) {
        this.client = client;
    }

    public TransferFormClient(@Autowired TransferDAOService transferDAOService,
                              @Autowired TransactionService transactionService,
                              @Autowired NumberAccountRepositoryService numberAccountRepositoryService) {
        this.numberAccountRepositoryService = numberAccountRepositoryService;
        this.transactionService = transactionService;
        this.transferDAOService = transferDAOService;
        historyTransfer = new Grid<>();
        historyTransferGrid();
        setSizeFull();
        updateHistoryItem();
        updateComboItem();
    }

    private void historyTransferGrid() {
        updateHistoryItem();
        historyTransfer.addColumn(Transfer::toString).setHeader("История");
        historyTransfer.getColumns().forEach(colum -> colum.setAutoWidth(true));
        historyTransfer.recalculateColumnWidths();
        historyTransfer.setSizeUndefined();

        add(formLayout(), historyTransfer);

    }

    private FormLayout formLayout() {
        FormLayout transfer = new FormLayout();
        sender.setLabel("Отправитель");
        recipient.setLabel("Получатель");
        sender.setWidth("300px");
        recipient.setWidth("300px");
        amount.setLabel("Введите сумму перевода");
        senderCombo.setLabel("Выбрать из списка");
        recipientCombo.setLabel("Выбрать из списка");
        senderCombo.setWidth("300px");
        recipientCombo.setWidth("300px");
        updateComboItem();

        recipientCombo.addValueChangeListener(event -> {
            recipient.setValue(event.getValue().getNumber());
        });
        senderCombo.addValueChangeListener(event -> {
            sender.setValue(event.getValue().getNumber());
        });

        VerticalLayout senderLayout = new VerticalLayout();
        senderLayout.add(sender, senderCombo);

        VerticalLayout recipientLayout = new VerticalLayout();
        recipientLayout.add(recipient, recipientCombo);

        transfer.add(senderLayout, recipientLayout, amount, createButton());
        return transfer;
    }

    private Component createButton() {
        Button transactionBtn = new Button("Перевод", event -> {
            if (!(sender.getValue().equals(recipient.getValue()))) {
                try {
                    transaction();
                    updateHistoryItem();
                } catch (FindAccountException e) {
                    Notification.show(e.getMessage());
                }
            } else Notification.show("Отправитель и получатель не может быть один и тот же");
        });
        transactionBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        return new HorizontalLayout(transactionBtn);
    }

    private void transaction() throws FindAccountException {
        if (checkNumberAccount(sender)
                && checkNumberAccount(recipient)
                && checkAmount()) {
            Transfer transfer = new Transfer(numberAccountRepositoryService.findAllByNumber(sender.getValue()),
                    numberAccountRepositoryService.findAllByNumber(recipient.getValue()),
                    amount.getValue());
            try {
                transactionService.transfer(transfer);
            } catch (FindAccountException e) {
                Notification.show(e.getMessage());
            } catch (SummException e) {
                Notification.show(e.getMessage());
            }
            updateHistoryItem();
        }
    }

    private boolean checkAmount() {
        return (amount != null) && !(amount.isEmpty());
    }

    private boolean checkNumberAccount(TextField textField) {
        Pattern p = Pattern.compile("[^0-9]");
        Matcher matcher = p.matcher(textField.getValue());
        if ((textField.getValue() != null) || !(textField.getValue().isEmpty())
                && !matcher.find()
                && ((textField.getValue().length() == 20) || (textField.getValue().length() == 16))) {
            return true;
        } else return false;
    }

    public void updateHistoryItem() {
        if (client != null)
            historyTransfer.setItems(transferDAOService.findAllByClient(client));
        sender.setValue("");
        recipient.setValue("");
        amount.setValue(new BigDecimal(0));
    }

    public void updateComboItem() {
        if (client != null) {
            numberAccountListAll = numberAccountRepositoryService.findAll();
            numberAccountListByClient = numberAccountRepositoryService.findAllByBankingAccount_ClientAndCreditCard_Client(client);
            recipientCombo.setItems(numberAccountListAll);
            senderCombo.setItems(numberAccountListByClient);
        }
        sender.setValue("");
        recipient.setValue("");
        amount.setValue(new BigDecimal(0));
    }
}
