package ru.sberbank.views.client.transfer;


import com.vaadin.flow.component.html.Div;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.person.Client;

@Service
public class TransactionClientView extends Div {
    private Client client;
    private TransferFormClient transferForm;

    public void setClient(Client client) {
        this.client = client;
        transferForm.setClient(client);
    }

    public TransactionClientView(@Autowired TransferFormClient transferFormClient) {
        this.transferForm = transferFormClient;
        transferForm.updateComboItem();
        transferFormClient.updateHistoryItem();
        add(transferForm);
    }
}
