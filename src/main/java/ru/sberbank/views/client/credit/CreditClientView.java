package ru.sberbank.views.client.credit;

import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.html.Div;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.person.Client;

@Service
public class CreditClientView extends Div {

    private Client client;
    private CreateCreditCard createCreditCard;
    private CheckAnswerCreditCards checkAnswerCreditCards;
    private CheckCreditCards checkCreditCards;

    public void setClient(Client client) {
        this.client = client;
        createCreditCard.setClient(client);

        checkAnswerCreditCards.setClient(client);
        checkAnswerCreditCards.updateAnswerCreditList();

        checkCreditCards.setClient(client);
        checkCreditCards.updateCreditCardSelect();
    }

    public CreditClientView(@Autowired CreateCreditCard createCreditCard,
                            @Autowired CheckAnswerCreditCards checkAnswerCreditCards,
                            @Autowired CheckCreditCards checkCreditCards) {
        this.checkAnswerCreditCards = checkAnswerCreditCards;
        this.checkCreditCards = checkCreditCards;
        this.createCreditCard = createCreditCard;
        setSizeFull();

        Accordion accordion = new Accordion();
        accordion.setWidth("1500px");
        accordion.add("Подать заявку на кредитную карту", createCreditCard);
        accordion.add("Проверить заявки на кредитную карту", checkAnswerCreditCards);
        accordion.add("Кредитные карты", checkCreditCards);
        accordion.setSizeFull();
        accordion.close();
        accordion.addOpenedChangeListener(openedChangeEvent -> {
            checkAnswerCreditCards.updateAnswerCreditList();
            checkCreditCards.updateCreditCardSelect();
        });
        add(accordion);

    }

}
