package ru.sberbank.views.client.credit;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.radiobutton.RadioGroupVariant;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.RequestCredit;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.serviceEntity.Currency;
import ru.sberbank.model.service.dbService.credit.RequestCreditRepositoryService;

import java.math.BigDecimal;

@Service
public class CreateCreditCard extends Div {

    private Client client;
    private Div value;
    private Currency currencyValue;
    private BigDecimalField limitField;
    private RadioButtonGroup<String> radioGroup;

    private RequestCreditRepositoryService requestCreditRepositoryService;

    public CreateCreditCard(@Autowired RequestCreditRepositoryService requestCreditRepositoryService) {
        setSizeFull();
        value = new Div();
        this.requestCreditRepositoryService = requestCreditRepositoryService;
        radioGroup = new RadioButtonGroup<>();
        radioGroup.setLabel("Выберите валюту");
        radioGroup.setItems("RUB", "EUR", "USD");
        radioGroup.addThemeVariants(RadioGroupVariant.LUMO_VERTICAL);
        limitField = new BigDecimalField("Введите сумму лимита");
        limitField.addThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT);
        Button createCreditCard = new Button("Подать заявку на кредитную карту");

        radioGroup.addValueChangeListener(event -> {
            selectCurrency(event);
        });

        createCreditCard.addClickListener(clik -> {
            if ((currencyValue != null) && (limitField.getValue() != null )) createRequest();
            else Notification.show("Выберите валюту и сумму");
        });
        add(radioGroup, value, limitField, createCreditCard);
    }

    public void setClient(Client client) {
        this.client = client;
    }

    private void selectCurrency(AbstractField.ComponentValueChangeEvent<RadioButtonGroup<String>, String> event) {
        if (event.getValue() == null) {
            value.setText("No option selected");
        } else {
            value.setText("Selected: " + event.getValue());
            currencyValue = Currency.valueOf(event.getValue());
        }
    }

    private void createRequest() {
        RequestCredit requestCredit = new RequestCredit(currencyValue, limitField.getValue(), client);
        requestCreditRepositoryService.save(requestCredit);
        limitField.setValue(new BigDecimal("0"));
    }


}
