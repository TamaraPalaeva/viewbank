package ru.sberbank.views.client.credit;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.FooterRow;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.AnswerCredit;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.service.dbService.credit.AnswerCreditRepositoryService;
import ru.sberbank.model.service.serverProcess.credit.CreditCardService;

@Service
public class CheckAnswerCreditCards extends Div {
    private Client client;
    private Grid<AnswerCredit> gridAnswer;
    private AnswerCreditRepositoryService answerCreditRepositoryService;
    private CreditCardService creditCardService;
    private AnswerCredit selectAnswerCredit;
    private Div messageAnswer;

    public CheckAnswerCreditCards(@Autowired AnswerCreditRepositoryService answerCreditRepositoryService,
                                  @Autowired CreditCardService creditCardService) {
        setSizeFull();
        this.answerCreditRepositoryService = answerCreditRepositoryService;
        this.creditCardService = creditCardService;
        gridAnswer = new Grid<>();
        createListCard();
        //Button createGrid = new Button("Обновить список", e -> createListCard());
        updateAnswerCreditList();
    }

    public void setClient(Client client) {
        this.client = client;
    }

    private void createListCard() {
        messageAnswer = new Div();
        Grid.Column<AnswerCredit> currency = gridAnswer.addColumn(AnswerCredit::getCurrency).setHeader("Валюта");
        Grid.Column<AnswerCredit> limit = gridAnswer.addColumn(answerCredit1 ->
                answerCredit1.getLimitCreditCard())
                .setHeader("Лимит по кредиту");
        gridAnswer.addColumn(answerCredit ->
                answerCredit.getPercentCredit())
                .setHeader("Процент по кредиту");
        gridAnswer.addColumn(answerCredit ->
                answerCredit.getPercentagePenalties())
                .setHeader("Пени");
        gridAnswer.getColumns()
                .forEach(column -> column.setAutoWidth(true));
        gridAnswer.recalculateColumnWidths();
        gridAnswer.setHeightFull();
        gridAnswer.setSizeUndefined();

        gridAnswer.asSingleSelect().addValueChangeListener(evt -> {
            if (evt.getValue() != null) {
                selectAnswerCredit = evt.getValue();
                String mess = String.format("Информация : \n %s",
                        evt.getValue().toString());
                messageAnswer.setText(mess);
            }
        });

        Button addCreditCard = new Button("Принять", event -> {
            selectAnswerCredit.setApproved(true);
            answerCreditRepositoryService.save(selectAnswerCredit);
            creditCardService.createCreditCard(selectAnswerCredit);
            gridAnswer.getDataProvider().refreshAll();
            gridAnswer.deselectAll();
            updateAnswerCreditList();
        });

        Button removeCreditCard = new Button("Отклонить", event -> {
            selectAnswerCredit.setApproved(true);
            creditCardService.createCreditCard(selectAnswerCredit);
            gridAnswer.getDataProvider().refreshAll();
            updateAnswerCreditList();
        });

        FooterRow footerRow = gridAnswer.appendFooterRow();
        footerRow.getCell(currency).setComponent(addCreditCard);
        footerRow.getCell(limit).setComponent(removeCreditCard);
        add(gridAnswer, addCreditCard, removeCreditCard, messageAnswer);
    }

    public void updateAnswerCreditList() {
        if (client != null) {
            gridAnswer.setItems(answerCreditRepositoryService.findByClientDontApproved(client));
            messageAnswer.setText("");
            selectAnswerCredit = null;
        }
    }
}
