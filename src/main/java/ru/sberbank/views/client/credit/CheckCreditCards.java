package ru.sberbank.views.client.credit;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.CreditCard;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.service.dbService.credit.CreditCardRepositoryService;
import ru.sberbank.model.service.serverProcess.credit.CreditCardService;

import java.text.SimpleDateFormat;

@Service
public class CheckCreditCards extends Div {
    private Client client;
    private Grid<CreditCard> creditCardGrid;
    private CreditCardRepositoryService creditCardRepositoryService;
    private CreditCardService creditCardService;
    private CreditCard selectCreditCard;
    private Div messageCard;

    public CheckCreditCards(@Autowired CreditCardRepositoryService creditCardRepositoryService,
                            @Autowired CreditCardService creditCardService) {
        this.creditCardService = creditCardService;
        this.creditCardRepositoryService = creditCardRepositoryService;
        setSizeFull();
        creditCardGrid = new Grid<>();
        itemsCard();
        updateCreditCardSelect();
    }

    private void itemsCard() {
        messageCard = new Div();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Grid.Column<CreditCard> numberCard = creditCardGrid.addColumn(CreditCard::getId_card).setHeader("Номер карты");
        Grid.Column<CreditCard> amountCard = creditCardGrid.addColumn(CreditCard::getAmountCredit).setHeader("Сумма кредита");
        creditCardGrid.addColumn(creditCard -> {
            if (creditCard.isActiveCredit()) return format.format(creditCard.getDateFinishCreditPeriod());
            else return "-";
        }).setHeader("Срок кредитования");
        creditCardGrid.getColumns()
                .forEach(column -> column.setAutoWidth(true));
        creditCardGrid.recalculateColumnWidths();
        creditCardGrid.setHeightFull();
        creditCardGrid.setSizeUndefined();
        creditCardGrid.asSingleSelect().addValueChangeListener(event -> {
            selectCreditCard = event.getValue();
            if (selectCreditCard != null) {
                String message = String.format("Информация по кредитной карте: \n %s",
                        selectCreditCard.toString());
                messageCard.setText(message);
            }
        });

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(creditCardGrid, messageCard, formLayout());

        add(verticalLayout);


    }

    private FormLayout formLayout() {
        FormLayout transactionLayout = new FormLayout();

        BigDecimalField summField = new BigDecimalField();
        summField.setLabel("Введите сумму");

        Button depositButton = new Button("Пополнить");
        Button creditButton = new Button("Снять");

        try {
            creditButton.addClickListener(click -> {
                creditCardService.creditTransactionWithCreditCard(selectCreditCard, summField.getValue());
                updateCreditCardSelect();
            });
            depositButton.addClickListener(event -> {
                creditCardService.debitTransactionWithCreditCard(selectCreditCard, summField.getValue());
                updateCreditCardSelect();
            });
        } catch (Exception e) {
            Notification.show(e.getMessage());
        }
        transactionLayout.add(summField, depositButton, creditButton);
        return transactionLayout;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void updateCreditCardSelect() {
        if (client != null) {
            creditCardGrid.setItems(creditCardRepositoryService.findAllByClientIsActiveCard(client));
            selectCreditCard = null;
            messageCard.setText("");
        }
    }
}
