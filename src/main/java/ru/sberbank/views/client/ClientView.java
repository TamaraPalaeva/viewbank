package ru.sberbank.views.client;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.*;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.service.dbService.service.AuthorizationRepositoryService;
import ru.sberbank.views.client.credit.CreditClientView;
import ru.sberbank.views.client.deposit.DepositClientView;
import ru.sberbank.views.client.transfer.TransactionClientView;
import ru.sberbank.views.client.transfer.TransferFormClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Route("client")
@CssImport("./styles/shared-styles.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
public class ClientView extends VerticalLayout implements HasUrlParameter<String> {
    private Client client;
    private H1 textWelcome;

    @Autowired
    private AuthorizationRepositoryService authorizationRepositoryService;

    @Override
    public void setParameter(BeforeEvent event,
                             @WildcardParameter String parameter) {
        Location location = event.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();

        Map<String, List<String>> parametersMap = queryParameters.getParameters();
        String clientId = parametersMap.get("login").get(0);
        client = authorizationRepositoryService.findByLogin(clientId).getClient();
        textWelcome.setText("Добро пожаловать, " + client.toString());
    }

    public ClientView(@Autowired DepositClientView depositClientView,
                      @Autowired CreditClientView creditClientView,
                      @Autowired TransactionClientView transactionClientView,
                      @Autowired TransferFormClient transferFormClient) {
        textWelcome = new H1();
        setSizeFull();
        setWidth("1500px");
        Tab deposit = new Tab("Депозитные счета");
        Tab credit = new Tab("Кредитные карты");
        Tab transfer = new Tab("Переводы");
        creditClientView.setVisible(false);
        transactionClientView.setVisible(false);
        depositClientView.setVisible(false);

        Map<Tab, Component> tabsToPages = new HashMap<>();
        tabsToPages.put(deposit, depositClientView);
        tabsToPages.put(credit, creditClientView);
        tabsToPages.put(transfer, transactionClientView);
        Tabs tabs = new Tabs(deposit, credit, transfer);
        tabs.setWidth("1500px");
        Div pages = new Div(depositClientView, creditClientView, transactionClientView);
        pages.setWidth("1500px");
        tabs.addSelectedChangeListener(event -> {
            pages.setVisible(true);
            tabsToPages.values().forEach(page -> page.setVisible(false));
            Component selectedPage = tabsToPages.get(tabs.getSelectedTab());
            selectedPage.setVisible(true);
            depositClientView.setClient(client);
            creditClientView.setClient(client);
            transactionClientView.setClient(client);
            if (event.getSelectedTab().toString().equals("Tab{Переводы}")) transferFormClient.updateComboItem();
        });

        add(textWelcome, tabs, pages);


    }


}
