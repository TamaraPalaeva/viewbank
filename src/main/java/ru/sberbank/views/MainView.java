package ru.sberbank.views;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A sample Vaadin view class.
 * <p>
 * To implement a Vaadin view just extend any Vaadin component and
 * use @Route annotation to announce it in a URL as a Spring managed
 * bean.
 * Use the @PWA annotation make the application installable on phones,
 * tablets and some desktop browsers.
 * <p>
 * A new instance of this class is created for every new user and every
 * browser tab/window.
 */

@Route
@CssImport("./styles/shared-styles.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
public class MainView extends VerticalLayout {

    public MainView(@Autowired GreetService service) {
        setSizeFull();
        TextField login = new TextField("Логин");
        login.setClearButtonVisible(true);
        login.addThemeName("bordered");

        PasswordField password = new PasswordField();
        password.setLabel("Пароль");
        service.createBD();

        Button button = new Button("Авторизация");

        button.addClickListener(e -> {
            String loginText = login.getValue();
            String passwordText = password.getValue();

                    if ( (loginText.length() > 0) && (passwordText.length() > 0)) {
                        String person = null;
                        try {
                            person = service.greet(login.getValue(), password.getValue());
                            if (person.equals("client")) {
                                button.getUI().ifPresent(ui -> {
                                    Map<String, List<String>> parametersMap = new HashMap<String, List<String>>();
                                    parametersMap.put("login", Arrays.asList(login.getValue()));
                                    QueryParameters queryParameters = new QueryParameters(parametersMap);
                                    ui.navigate("client", queryParameters);
                                });
                            } else button.getUI().ifPresent(ui -> ui.navigate("operator"));
                        } catch (Exception exception) {
                            Notification.show("Неверно введен логин и пароль");
                            Notification.show(exception.toString());
                        }
                    }
                    else {
                        Notification.show("Неверно введена пара логин и пароль");
                    }
                }
        );


        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        button.addClickShortcut(Key.ENTER);

        addClassName("centered-content");

        add(login, password, button);
    }

}
