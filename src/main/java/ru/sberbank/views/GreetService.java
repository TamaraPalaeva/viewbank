package ru.sberbank.views;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.CreateBD;
import ru.sberbank.model.service.serverProcess.AuthorizationService;

import java.io.Serializable;


@Service
public class GreetService implements Serializable {

    @Autowired
    private AuthorizationService authorizationService;
    @Autowired
    private CreateBD createBD;

    public String greet(String login, String password) {
        return authorizationService.openEntity(login, password);
    }

    public void createBD() {
        createBD.createBD();
    }

}
