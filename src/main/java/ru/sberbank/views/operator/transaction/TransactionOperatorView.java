package ru.sberbank.views.operator.transaction;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.transfer.Transfer;
import ru.sberbank.model.service.dbService.transfer.TransferDAOService;
import ru.sberbank.model.service.serverProcess.transfer.TransactionService;

@Service
public class TransactionOperatorView extends Div {
    private TransferDAOService transferDAOService;
    private TransactionService transactionService;
    private Grid<Transfer> transferGrid;
    private Transfer selectTransfer;

    public TransactionOperatorView(@Autowired TransferDAOService transferDAOService,
                                   @Autowired TransactionService transactionService) {
        Button findAllButton = new Button("Обновить список", clik -> updateList());
        this.transferDAOService = transferDAOService;
        this.transactionService = transactionService;
        setSizeFull();
        setWidth("1500px");
        transferGrid = new Grid<>();
        createTransferGrid();
        add(transferGrid, createButton());

    }

    private HorizontalLayout createButton() {
        Button ok = new Button("OK", clik -> transactionOk());
        Button cancel = new Button("Отменить", clik -> transactionCancel());
        HorizontalLayout toolbar = new HorizontalLayout(ok,cancel);
        return toolbar;
    }

    private void transactionCancel() {
        selectTransfer.setIsDone(true);
        transferDAOService.update(selectTransfer);
    }

    private void transactionOk() {
        transactionService.transferIsFrod(selectTransfer);
    }

    private void createTransferGrid() {
        updateList();
        transferGrid.addColumn(col -> col.getSender()).setHeader("Отправитель");
        transferGrid.addColumn(col -> col.getRecipient()). setHeader("Получатель");
        transferGrid.addColumn(col -> col.getAmount()).setHeader("Сумма перевода");
        transferGrid.recalculateColumnWidths();
        transferGrid.setHeightFull();
        transferGrid.setSizeUndefined();
        transferGrid.asSingleSelect().addValueChangeListener(event -> {
            selectTransfer = event.getValue();
        });
    }

    public void updateList() {
        transferGrid.setItems(transferDAOService.findAllByIsFrod());
    }
}
