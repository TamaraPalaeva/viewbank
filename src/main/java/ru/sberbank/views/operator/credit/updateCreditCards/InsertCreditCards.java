package ru.sberbank.views.operator.credit.updateCreditCards;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.CreditCard;
import ru.sberbank.model.service.dbService.credit.CreditCardRepositoryService;

@Service
public class InsertCreditCards extends VerticalLayout {
    private CreditCardRepositoryService creditCardRepositoryService;
    private CreateCreditCardForm createCreditCardForm;
    private Grid<CreditCard> gridCreditCard;

    public InsertCreditCards(@Autowired CreditCardRepositoryService creditCardRepositoryService,
                             @Autowired CreateCreditCardForm createCreditCardForm) {
        this.creditCardRepositoryService = creditCardRepositoryService;
        this.createCreditCardForm = createCreditCardForm;
        setSizeFull();
        gridCreditCard = new Grid<>();
        createGridCreditCard();
        updateList();
        Div creditCardDiv = new Div(gridCreditCard, createCreditCardForm);
        creditCardDiv.setSizeFull();
        add(creditCardDiv);
    }


    private void createGridCreditCard() {
        updateList();
        closeEditor();
        gridCreditCard.addColumn(col -> col.getId_card()).setHeader("Номер карты");
        gridCreditCard.addColumn(col -> col.getClient().toString()).setHeader("Владелец карты");
        gridCreditCard.recalculateColumnWidths();
        gridCreditCard.setHeightFull();
        gridCreditCard.setSizeUndefined();
        gridCreditCard.asSingleSelect().addValueChangeListener(evt -> {
            editRequest(evt.getValue());});
    }

    private void editRequest(CreditCard value) {
        if (value == null){
            closeEditor();
        } else {
            createCreditCardForm.setCreditCard(value);
            createCreditCardForm.addElementForm();
            createCreditCardForm.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor() {
        createCreditCardForm.setVisible(false);
        removeClassName("editing");
        gridCreditCard.deselectAll();
        updateList();
    }

    public void updateList() {
        gridCreditCard.setItems(creditCardRepositoryService.findAll());
    }
}
