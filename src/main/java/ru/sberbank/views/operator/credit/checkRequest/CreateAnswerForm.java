package ru.sberbank.views.operator.credit.checkRequest;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.shared.Registration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.AnswerCredit;
import ru.sberbank.model.entity.credit.RequestCredit;
import ru.sberbank.model.service.dbService.credit.AnswerCreditRepositoryService;
import ru.sberbank.model.service.dbService.credit.RequestCreditRepositoryService;

import java.math.BigDecimal;

@Service
public class CreateAnswerForm extends FormLayout {
    private RequestCredit requestCredit;
    private AnswerCreditRepositoryService answerCreditRepositoryService;
    private RequestCreditRepositoryService requestCreditRepositoryService;

    TextArea limit = new TextArea("Лимит по карте");
    TextArea currency = new TextArea("Валюта");
    BigDecimalField percentCredit = new BigDecimalField("Процент по кредиту");
    BigDecimalField percentagePenalties = new BigDecimalField("Процент пени");
    TextArea clientField = new TextArea("Данные клиента");

    Button save = new Button("Одобрить запрос");
    Button remove = new Button("Отклонить запрос");


    public CreateAnswerForm(@Autowired AnswerCreditRepositoryService answerCreditRepositoryService,
                            @Autowired RequestCreditRepositoryService requestCreditRepositoryService) {
        this.answerCreditRepositoryService = answerCreditRepositoryService;
        this.requestCreditRepositoryService = requestCreditRepositoryService;
        addClassName("contact-form");

        add(clientField,
                limit,
                currency,
                percentCredit,
                percentagePenalties,
                createButtonsLayout());
    }

    public void addElementForm() {
        limit.setValue(requestCredit.getLimitCreditCard().toString());
        currency.setValue(requestCredit.getCurrency().toString());
        clientField.setValue(requestCredit.getClient().toString());
    }

    public void setRequestCredit(RequestCredit requestCredit) {
        this.requestCredit = requestCredit;
    }

    private Component createButtonsLayout() {
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        remove.addThemeVariants(ButtonVariant.LUMO_ERROR);

        save.addClickShortcut(Key.ENTER);

        save.addClickListener(click -> {
            saveAnswer();
        });
        remove.addClickListener(click -> {
            removeRequest();
        });

        return new HorizontalLayout(save, remove);
    }

    private void removeRequest() {
        requestCredit.setWorkedOut(true);
        requestCreditRepositoryService.update(requestCredit);
        closeForm();
    }

    private void saveAnswer() {
        if ((percentagePenalties.getValue() != null) && (percentCredit.getValue() != null)) {
            AnswerCredit answerCredit = new AnswerCredit(requestCredit.getLimitCreditCard(),
                    requestCredit.getCurrency(),
                    percentCredit.getValue(),
                    percentagePenalties.getValue(),
                    requestCredit.getClient());
            requestCredit.setWorkedOut(true);
            requestCreditRepositoryService.update(requestCredit);
            answerCreditRepositoryService.save(answerCredit);
            closeForm();
        } else {
            Notification.show("Не введены все параметры");
        }
    }

    private void closeForm() {
        this.setVisible(false);
        limit.setValue("");
        clientField.setValue("");
        currency.setValue("");
        percentagePenalties.setValue(new BigDecimal("0"));
        percentCredit.setValue(new BigDecimal("0"));
        fireEvent(new CloseEvent(this));
    }

    public static abstract class ContactFormEvent extends ComponentEvent<CreateAnswerForm> {
        protected ContactFormEvent(CreateAnswerForm source) {
            super(source, false);
        }
    }

    public static class CloseEvent extends ContactFormEvent {
        CloseEvent(CreateAnswerForm source) {
            super(source);
        }
    }


    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }

}
