package ru.sberbank.views.operator.credit.checkRequest;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.RequestCredit;
import ru.sberbank.model.service.dbService.credit.RequestCreditRepositoryService;

@Service
public class CheckRequestCreditCards extends VerticalLayout {

    private RequestCreditRepositoryService requestCreditRepositoryService;
    private Grid<RequestCredit> gridRequest;
    private RequestCredit requestCredit;
    private CreateAnswerForm createAnswerForm;

    public CheckRequestCreditCards(@Autowired CreateAnswerForm createAnswerForm,
                                   @Autowired RequestCreditRepositoryService requestCreditRepositoryService) {
        this.requestCreditRepositoryService = requestCreditRepositoryService;
        this.createAnswerForm = createAnswerForm;
        setSizeFull();
        gridRequest = new Grid<>();
        createGridRequest();
        Div content = new Div(gridRequest, createAnswerForm);
        content.addClassName("content");
        content.setSizeFull();
        createAnswerForm.addListener(CreateAnswerForm.CloseEvent.class, e -> updateList());
        add(content);
    }

    private void createGridRequest() {
        updateList();
        closeEditor();
        gridRequest.addColumn(col ->
                col.getClient().toString())
                .setHeader("Клиент");
        gridRequest.addColumn(col -> col.getCurrency()).setHeader("Валюта");
        gridRequest.addColumn(col -> col.getLimitCreditCard()).setHeader("Лимит по карте");
        gridRequest.getColumns()
                .forEach(column -> column.setAutoWidth(true));
        gridRequest.recalculateColumnWidths();
        gridRequest.setHeightFull();
        gridRequest.setSizeUndefined();
        gridRequest.asSingleSelect().addValueChangeListener(evt -> {
            editRequest(evt.getValue());});

    }

    private void editRequest(RequestCredit value) {
        if (value == null){
            closeEditor();
        } else {
            createAnswerForm.setRequestCredit(value);
            createAnswerForm.addElementForm();
            createAnswerForm.setVisible(true);
            addClassName("editing");
        }
    }

    public void updateList() {
        gridRequest.setItems(requestCreditRepositoryService.findAllByWorkOut());
    }

    private void closeEditor() {
        createAnswerForm.setRequestCredit(null);
        createAnswerForm.setVisible(false);
        removeClassName("editing");
    }
}
