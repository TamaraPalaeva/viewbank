package ru.sberbank.views.operator.credit.updateCreditCards;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.TextArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.CreditCard;
import ru.sberbank.model.service.dbService.credit.CreditCardRepositoryService;

import java.math.BigDecimal;

@Service
public class CreateCreditCardForm extends FormLayout {
    private CreditCard creditCard;
    private CreditCardRepositoryService creditCardRepositoryService;

    private TextArea numberCreditCard = new TextArea("Номер карты");
    private TextArea clientField = new TextArea("Данные клиента");
    private TextArea limit = new TextArea("Лимит по карте");
    private TextArea currency = new TextArea("Валюта");
    private BigDecimalField percentCredit = new BigDecimalField("Процент по кредиту");
    private BigDecimalField percentagePenalties = new BigDecimalField("Процент пени");

    private Button save = new Button("Внести изменения");
    private Button cancel = new Button("Отмена");


    public CreateCreditCardForm(@Autowired CreditCardRepositoryService creditCardRepositoryService) {
        this.creditCardRepositoryService = creditCardRepositoryService;
        add(numberCreditCard,
                clientField,
                limit,
                currency,
                percentCredit,
                percentagePenalties,
                createButtonsLayout()
        );
    }

    private Component createButtonsLayout() {
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        cancel.addThemeVariants(ButtonVariant.LUMO_ERROR);

        save.addClickShortcut(Key.ENTER);

        save.addClickListener(click -> saveCreditCard());
        cancel.addClickListener(click -> closeForm());

        return new HorizontalLayout(save, cancel);
    }

    private void closeForm() {
        this.setVisible(false);
        numberCreditCard.setValue("");
        limit.setValue("");
        clientField.setValue("");
        currency.setValue("");
        percentagePenalties.setValue(new BigDecimal("0"));
        percentCredit.setValue(new BigDecimal("0"));
    }

    private void saveCreditCard() {
        if ((percentagePenalties.getValue() != null) && (percentCredit.getValue() != null)){
        creditCard.setPercentagePenalties(percentagePenalties.getValue());
            creditCard.setPercentCredit(percentCredit.getValue());
            creditCardRepositoryService.update(creditCard);
            closeForm();}
        else {
            Notification.show("Не введены все параметры");
        }
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public void addElementForm() {
        numberCreditCard.setValue(creditCard.getId_card().toString());
        limit.setValue(creditCard.getLimitCredit().toString());
        clientField.setValue(creditCard.getClient().toString());
        currency.setValue(creditCard.getCurrency().toString());
        percentagePenalties.setValue(new BigDecimal(creditCard.getPercentagePenalties().toString()));
        percentCredit.setValue(new BigDecimal(creditCard.getPercentCredit().toString()));
    }


}
