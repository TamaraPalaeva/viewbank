package ru.sberbank.views.operator.credit;

import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.html.Div;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.views.operator.credit.checkRequest.CheckRequestCreditCards;
import ru.sberbank.views.operator.credit.updateCreditCards.InsertCreditCards;

@Service
public class CreditOperatorView extends Div {

    public CreditOperatorView(@Autowired CheckRequestCreditCards checkRequestCreditCards,
                              @Autowired InsertCreditCards insertCreditCards) {
        setSizeFull();
        Accordion accordion = new Accordion();
        accordion.add("Проверить заявки на кредитную карту", checkRequestCreditCards);
        accordion.add("Редактирование условий кредитования", insertCreditCards);
        accordion.close();
        accordion.addOpenedChangeListener(openedChangeEvent -> {
            insertCreditCards.updateList();
            checkRequestCreditCards.updateList();
        });
        accordion.setSizeFull();
        add(accordion);
    }
}
