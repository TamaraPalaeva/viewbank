package ru.sberbank.views.operator.deposit;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.deposit.RateOnDeposit;
import ru.sberbank.model.service.dbService.deposit.RateOnDepositRepositoryService;

@Service
public class DepositOperatorView extends Div {
    private RateOnDepositRepositoryService rateOnDepositRepositoryService;
    private Grid<RateOnDeposit> rateOnDepositGrid;
    private CreateAccountForm createAccountForm;

    public DepositOperatorView(@Autowired RateOnDepositRepositoryService rateOnDepositRepositoryService,
                               @Autowired CreateAccountForm createAccountForm) {
        this.rateOnDepositRepositoryService = rateOnDepositRepositoryService;
        this.createAccountForm = createAccountForm;
        setSizeFull();
        setWidth("1500px");
        rateOnDepositGrid = new Grid<>();
        createBankingAccountGrid();
        updateList();
        Div accountDiv = new Div(rateOnDepositGrid, createAccountForm);
        add(getButton(), accountDiv);
    }

    private HorizontalLayout getButton() {
        Button addButton = new Button("Добавить тариф", clik -> addRate());
        HorizontalLayout toolBar = new HorizontalLayout(addButton);
        toolBar.addClassName("toolbar");
        return toolBar;
    }

    private void addRate() {
        createAccountForm.closeForm();
        createAccountForm.addRate();
        createAccountForm.setVisible(true);
    }

    private void createBankingAccountGrid() {
        updateList();
        closeEditor();
        rateOnDepositGrid.addColumn(col -> col.getDurationOfDeposit()).setHeader("Срок по вкладу");
        rateOnDepositGrid.addColumn(col -> col.getPercentDeposit()).setHeader("Процент по вкладу");
        rateOnDepositGrid.recalculateColumnWidths();
        rateOnDepositGrid.setHeightFull();
        rateOnDepositGrid.setSizeUndefined();
        rateOnDepositGrid.asSingleSelect().addValueChangeListener(evt -> {
            editAccountForm(evt.getValue());
        });
    }

    private void editAccountForm(RateOnDeposit value) {
        if (value == null) {
            closeEditor();
        } else {
            createAccountForm.setRate(value);
            createAccountForm.addElementForm();
            createAccountForm.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor() {
        createAccountForm.setVisible(false);
    }

    public void updateList() {
        rateOnDepositGrid.setItems(rateOnDepositRepositoryService.findAll());
    }
}
