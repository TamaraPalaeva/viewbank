package ru.sberbank.views.operator.deposit;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.deposit.RateOnDeposit;
import ru.sberbank.model.entity.serviceEntity.DurationOfDeposit;
import ru.sberbank.model.service.dbService.deposit.RateOnDepositRepositoryService;
import ru.sberbank.model.service.dbService.service.DurationOfDepositRepositoryService;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CreateAccountForm extends HorizontalLayout {
    private RateOnDeposit rateOnDeposit;
    private DurationOfDepositRepositoryService durationOfDepositRepositoryService;
    private RateOnDepositRepositoryService rateOnDepositRepositoryService;
    private boolean isNew;

    private BigDecimalField minAmount = new BigDecimalField("Минимальная сумма вклада");
    private BigDecimalField maxAmount = new BigDecimalField("Максимальная сумма вклада");
    private ComboBox<DurationOfDeposit> duration = new ComboBox<>("Срок вклада");
    private Checkbox isRefill = new Checkbox();
    private Checkbox isClose = new Checkbox();
    private Checkbox isCapital = new Checkbox();
    private Checkbox isCharge = new Checkbox();

    private BigDecimalField percent = new BigDecimalField("Процент по вкладу");

    private Button ok = new Button("OK");
    private Button cancel = new Button("Отмена");
    private Button delete = new Button("Удалить");

    public CreateAccountForm(@Autowired DurationOfDepositRepositoryService durationOfDepositRepositoryService,
                             @Autowired RateOnDepositRepositoryService rateOnDepositRepositoryService) {
        this.durationOfDepositRepositoryService = durationOfDepositRepositoryService;
        this.rateOnDepositRepositoryService = rateOnDepositRepositoryService;
        add(minAmount,
                maxAmount,
                percent,
                duration,
                addCheckBox());
        add(
                createButton()
        );
    }

    private Component createButton() {
        ok.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        cancel.addThemeVariants(ButtonVariant.LUMO_ERROR);

        ok.addClickListener(event -> saveRate());
        cancel.addClickListener(event -> closeForm());
        delete.addClickListener(event -> deleteRate());
        return new HorizontalLayout(ok, delete, cancel);
    }

    private void deleteRate() {
        rateOnDepositRepositoryService.delete(rateOnDeposit);
        closeForm();

    }

    private void saveRate() {
        rateOnDeposit.setMinAmount(minAmount.getValue());
        rateOnDeposit.setMaxAmount(maxAmount.getValue());
        rateOnDeposit.setPercentDeposit(percent.getValue());

        if ((rateOnDeposit.getMinAmount() != null && rateOnDeposit.getMaxAmount() != null
                && rateOnDeposit.getPercentDeposit() != null && rateOnDeposit.getDurationOfDeposit() != null)
                && (rateOnDeposit.getMaxAmount().compareTo(rateOnDeposit.getMinAmount()) == 1)) {
            if (isNew) rateOnDepositRepositoryService.save(rateOnDeposit);
            else rateOnDepositRepositoryService.update(rateOnDeposit);
        } else Notification.show("Введите параметры");

        closeForm();

    }

    public void closeForm() {
        this.setVisible(false);
        maxAmount.setValue(new BigDecimal(0));
        minAmount.setValue(new BigDecimal(0));
        percent.setValue(new BigDecimal(0));
        isCapital.setValue(false);
        isCharge.setValue(false);
        isClose.setValue(false);
        isRefill.setValue(false);
        duration.setValue(null);
    }

    public void setRate(RateOnDeposit value) {
        rateOnDeposit = value;
        isNew = false;
        delete.setVisible(true);
        addComboBox();
        addItemsComboBox();
    }

    public void addRate() {
        rateOnDeposit = new RateOnDeposit();
        isNew = true;
        delete.setVisible(false);
        addComboBox();
        addItemsComboBox();
    }

    public void addElementForm() {
        minAmount.setValue(rateOnDeposit.getMinAmount());
        maxAmount.setValue(rateOnDeposit.getMaxAmount());
        percent.setValue(rateOnDeposit.getPercentDeposit());

        isRefill.setValue(rateOnDeposit.isRefill());
        isClose.setValue(rateOnDeposit.isClose());
        isCharge.setValue(rateOnDeposit.isCharge());
        isCapital.setValue(rateOnDeposit.isCapital());
        addComboBox();

    }

    private Component addCheckBox() {
        isCapital.setLabel("Капитализируемый");
        isCharge.setLabel("Досрочное снятие денег");
        isClose.setLabel("Досрочное закрытие");
        isRefill.setLabel("Возможное пополнение");

        isRefill.addValueChangeListener(event -> rateOnDeposit.setRefill(event.getValue()));
        isClose.addValueChangeListener(event -> rateOnDeposit.setClose(event.getValue()));
        isCharge.addValueChangeListener(event -> rateOnDeposit.setCharge(event.getValue()));
        isCapital.addValueChangeListener(event -> rateOnDeposit.setCapital(event.getValue()));


        return new VerticalLayout(isCapital, isRefill, isCharge, isClose);
    }

    private void addComboBox() {
        duration.setVisible(true);
        duration.addValueChangeListener(event -> {
            DurationOfDeposit vail = event.getValue();
            rateOnDeposit.setDurationOfDeposit(vail);
        });
    }

    private void addItemsComboBox() {
        List<DurationOfDeposit> listDuration = durationOfDepositRepositoryService.findAll();
        duration.setItemLabelGenerator(DurationOfDeposit::toString);
        duration.setItems(listDuration);
        duration.setValue(rateOnDeposit.getDurationOfDeposit());
    }
}
