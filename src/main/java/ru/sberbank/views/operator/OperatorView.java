package ru.sberbank.views.operator;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.views.operator.credit.CreditOperatorView;
import ru.sberbank.views.operator.deposit.DepositOperatorView;
import ru.sberbank.views.operator.transaction.TransactionOperatorView;

import java.util.HashMap;
import java.util.Map;


@Route("operator")
@CssImport("./styles/shared-styles.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
public class OperatorView extends VerticalLayout {
    private Div textWelcome;

    private OperatorView(@Autowired DepositOperatorView depositOperatorView,
                         @Autowired CreditOperatorView creditOperatorView,
                         @Autowired TransactionOperatorView transactionOperatorView){
        textWelcome = new Div();
        setSizeFull();
        setWidth("1500px");
        Tab deposit = new Tab("Депозитные счета");
        Tab credit = new Tab("Кредитные карты");
        Tab transfer = new Tab("Переводы");
        creditOperatorView.setVisible(false);
        depositOperatorView.setVisible(false);
        transactionOperatorView.setVisible(false);

        Map<Tab, Component> tabsToPages = new HashMap<>();
        tabsToPages.put(deposit, depositOperatorView);
        tabsToPages.put(credit, creditOperatorView);
        tabsToPages.put(transfer, transactionOperatorView);
        Tabs tabs = new Tabs(deposit, credit, transfer);
        Div pages = new Div(depositOperatorView, creditOperatorView, transactionOperatorView);
        tabs.setWidth("1500px");
        tabs.addSelectedChangeListener(event -> {
            tabsToPages.values().forEach(page -> page.setVisible(false));
            Component selectedPage = tabsToPages.get(tabs.getSelectedTab());
            selectedPage.setVisible(true);
            depositOperatorView.updateList();
            transactionOperatorView.updateList();
        });

        add(textWelcome,tabs, pages);
    }
}
