package ru.sberbank.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.AnswerCredit;
import ru.sberbank.model.entity.credit.RequestCredit;
import ru.sberbank.model.entity.deposit.RateOnDeposit;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.person.Passport;
import ru.sberbank.model.entity.serviceEntity.*;
import ru.sberbank.model.service.dbService.ClientRepositoryService;
import ru.sberbank.model.service.dbService.PassportRepositoryService;
import ru.sberbank.model.service.dbService.credit.AnswerCreditRepositoryService;
import ru.sberbank.model.service.dbService.credit.RequestCreditRepositoryService;
import ru.sberbank.model.service.dbService.deposit.RateOnDepositRepositoryService;
import ru.sberbank.model.service.dbService.service.AuthorizationRepositoryService;
import ru.sberbank.model.service.dbService.service.CurrencyValueRepositoryService;
import ru.sberbank.model.service.dbService.service.DurationOfDepositRepositoryService;
import ru.sberbank.model.service.serverProcess.credit.CreditCardService;

import java.math.BigDecimal;

@Service
public class CreateBD {
    @Autowired
    private PassportRepositoryService passportRepositoryService;
    @Autowired
    private ClientRepositoryService clientRepositoryService;
    @Autowired
    private AuthorizationRepositoryService authorizationRepositoryService;
    @Autowired
    private AnswerCreditRepositoryService answerCreditRepositoryService;
    @Autowired
    private RequestCreditRepositoryService requestCreditRepositoryService;
    @Autowired
    private CreditCardService creditCardService;
    @Autowired
    private CurrencyValueRepositoryService currencyValueRepositoryService;
    @Autowired
    private DurationOfDepositRepositoryService durationOfDepositRepositoryService;
    @Autowired
    private RateOnDepositRepositoryService rateOnDepositRepositoryService;

    private boolean createdBD;

    public void createBD() {
        if (!(createdBD)) {
            createEntitlement();
        }
    }

    private void createEntitlement() {
        createdBD = true;
        Passport passport = new Passport(3612, 789562, "УВД Промышленного района");
        passportRepositoryService.save(passport);
        Passport passport1 = new Passport(3612, 123654, "УВД Промышленного района");
        passportRepositoryService.save(passport1);

        Client client = new Client("Иванов", "Иван", "Иванович", passport);
        clientRepositoryService.save(client);
        Client client1 = new Client("Петров", "Петр", "Олегович", passport1);
        clientRepositoryService.save(client1);

        Authorization authorization = new Authorization("Ivanov", "123", client);
        authorizationRepositoryService.save(authorization);
        Authorization authorization1 = new Authorization("Petorv", "123", client1);
        authorizationRepositoryService.save(authorization1);
        Authorization operationAuthorization = new Authorization("operator", "123");
        authorizationRepositoryService.save(operationAuthorization);

        AnswerCredit answerCredit = new AnswerCredit(new BigDecimal("10000.00"), Currency.RUB,
                new BigDecimal("18.00"),new BigDecimal("2.54"), client);
        answerCreditRepositoryService.save(answerCredit);

        RequestCredit requestCredit = new RequestCredit(Currency.RUB, new BigDecimal("15800.00"), client1);
        requestCreditRepositoryService.save(requestCredit);

        AnswerCredit answerCredit1 = new AnswerCredit(new BigDecimal("16500"), Currency.USD, new BigDecimal(10.50), new BigDecimal(1.05), client1);
        creditCardService.createCreditCard(answerCredit1);
        AnswerCredit answerCredit2 = new AnswerCredit(new BigDecimal("16500"), Currency.USD, new BigDecimal(10.50), new BigDecimal(1.05), client);
        creditCardService.createCreditCard(answerCredit2);

        currencyValueRepositoryService.save(new CurrencyValue(Currency.RUB, new BigDecimal(1), new BigDecimal(1)));
        currencyValueRepositoryService.save(new CurrencyValue(Currency.USD, new BigDecimal(74.39), new BigDecimal(0.013)));
        currencyValueRepositoryService.save(new CurrencyValue(Currency.EUR, new BigDecimal(90.37), new BigDecimal(0.011)));

        DurationOfDeposit duration1 = new DurationOfDeposit("3 года", NamePeriod.YEAR, 3);
        durationOfDepositRepositoryService.save(duration1);
        DurationOfDeposit duration2 = new DurationOfDeposit("1 год", NamePeriod.YEAR, 1);
        durationOfDepositRepositoryService.save(duration2);
        DurationOfDeposit duration3 = new DurationOfDeposit("3 месяца", NamePeriod.MONTH, 3);
        durationOfDepositRepositoryService.save(duration3);
        DurationOfDeposit duration4 = new DurationOfDeposit("6 месяцев", NamePeriod.MONTH, 6);
        durationOfDepositRepositoryService.save(duration4);

        RateOnDeposit rateOnDeposit = new RateOnDeposit(new BigDecimal(100_000), new BigDecimal(30_000),
                duration1, new BigDecimal(6.5), true, false, true, true);
        RateOnDeposit rateOnDeposit1 = new RateOnDeposit(new BigDecimal(300_000), new BigDecimal(100_000),
                duration2, new BigDecimal(4.8), true, true, true, true);
        RateOnDeposit rateOnDeposit3 = new RateOnDeposit(new BigDecimal(1_000_000), new BigDecimal(300_000),
                duration3, new BigDecimal(2.1), false, false, false, true);
        RateOnDeposit rateOnDeposit4 = new RateOnDeposit(new BigDecimal(100_000), new BigDecimal(0),
                duration4, new BigDecimal(0.1), true, true, true, true);
        rateOnDepositRepositoryService.save(rateOnDeposit);
        rateOnDepositRepositoryService.save(rateOnDeposit1);
        rateOnDepositRepositoryService.save(rateOnDeposit3);
        rateOnDepositRepositoryService.save(rateOnDeposit4);


    }
}
