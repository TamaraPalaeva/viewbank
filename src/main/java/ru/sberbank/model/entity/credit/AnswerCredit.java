package ru.sberbank.model.entity.credit;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.serviceEntity.Currency;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@ToString
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "answer_credits")
public class AnswerCredit {

    @Id
    @Column(name = "id_request")
    private String idrequest;

    private boolean approved = false;

    @Column(name = "limit_credit_card")
    private BigDecimal limitCreditCard;
    @Column(name = "currency")
    private Currency currency;
    @Column(name = "percent_credit")
    private BigDecimal percentCredit;
    @Column(name = "percentage_penalties")
    private BigDecimal percentagePenalties;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id")
    private Client client;

    public AnswerCredit() {

    }

    public AnswerCredit(BigDecimal limitCreditCard, Currency currency,
                        BigDecimal percentCredit, BigDecimal percentagePenalties, Client client) {
        this.idrequest = UUID.randomUUID().toString();
        this.limitCreditCard = limitCreditCard;
        this.currency = currency;
        this.percentCredit = percentCredit;
        this.percentagePenalties = percentagePenalties;
        this.client = client;
    }

    public AnswerCredit(String idrequest, boolean approved, BigDecimal limitCreditCard,
                        Currency currency, BigDecimal percentCredit, BigDecimal percentagePenalties, Client client) {
        this.idrequest = idrequest;
        this.approved = approved;
        this.limitCreditCard = limitCreditCard;
        this.currency = currency;
        this.percentCredit = percentCredit;
        this.percentagePenalties = percentagePenalties;
        this.client = client;
    }

    public String getIdrequest() {
        return idrequest;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public BigDecimal getLimitCreditCard() {
        return limitCreditCard;
    }

    public void setLimitCreditCard(BigDecimal limitCredit) {
        this.limitCreditCard = limitCredit;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getPercentCredit() {
        return percentCredit;
    }

    public void setPercentCredit(BigDecimal percentCredit) {
        this.percentCredit = percentCredit;
    }

    public BigDecimal getPercentagePenalties() {
        return percentagePenalties;
    }

    public void setPercentagePenalties(BigDecimal percentagePenalties) {
        this.percentagePenalties = percentagePenalties;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }


    @Override
    public String toString() {
        return "лимит по кредиту = " + limitCreditCard.toString() +
                ", валюта = " + currency.toString() +
                ", процент по кредиту = " + percentCredit.toString() +
                ", процент за просрочку = " + percentagePenalties.toString();
    }
}
