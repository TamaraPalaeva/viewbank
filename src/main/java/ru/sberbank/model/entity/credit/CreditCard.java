package ru.sberbank.model.entity.credit;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.serviceEntity.Currency;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@ToString
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "credit_cards")
public class CreditCard {
    @Id
    private String idCreditCard;
    @OneToOne
    @JoinColumn(name = "number_card")
    private NumberAccount numberCard;
    private String periodCard;
    //@Column(name = "isActive")
    private boolean isActiveCredit;
    private boolean isActiveCard;
    @Column(name = "date_finish_credit_period")
    private Date dateFinishCreditPeriod;
    @Column(name = "limit_credit")
    private BigDecimal limitCredit;
    @Column(name = "amount_credit")
    private BigDecimal amountCredit;
    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private Currency currency;
    @Column(name = "percent_credit")
    private BigDecimal percentCredit;
    @Column(name = "percentage_penalties")
    private BigDecimal percentagePenalties;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id")
    private Client client;


    public CreditCard() {
    }

    public CreditCard(NumberAccount number_card, String periodCard, Date dateFinishCreditPeriod,
                      BigDecimal limitCredit, BigDecimal amountCredit, Currency currency, BigDecimal percentCredit,
                      BigDecimal percentagePenalties, Client client) {
        this.idCreditCard = UUID.randomUUID().toString();
        this.numberCard = number_card;
        this.periodCard = periodCard;
        this.isActiveCredit = false;
        this.isActiveCard = true;
        this.dateFinishCreditPeriod = dateFinishCreditPeriod;
        this.limitCredit = limitCredit;
        this.amountCredit = amountCredit;
        this.currency = currency;
        this.percentCredit = percentCredit;
        this.percentagePenalties = percentagePenalties;
        this.client = client;
    }

    public NumberAccount getId_card() {
        return numberCard;
    }

    public boolean isActiveCredit() {
        return isActiveCredit;
    }

    public void setActiveCredit(boolean activeCredit) {
        isActiveCredit = activeCredit;
    }

    public String getPeriodCard() {
        return periodCard;
    }

    public void setPeriodCard(String periodCard) {
        this.periodCard = periodCard;
    }

    public Date getDateFinishCreditPeriod() {
        return dateFinishCreditPeriod;
    }

    public void setDateFinishCreditPeriod(Date dateBefore) {
        this.dateFinishCreditPeriod = dateBefore;
    }

    public BigDecimal getLimitCredit() {
        return limitCredit;
    }

    public void setLimitCredit(BigDecimal limitCredit) {
        this.limitCredit = limitCredit;
    }

    public BigDecimal getAmountCredit() {
        return amountCredit;
    }

    public void setAmountCredit(BigDecimal amountCredit) {
        this.amountCredit = amountCredit;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getPercentCredit() {
        return percentCredit;
    }

    public void setPercentCredit(BigDecimal percentCredit) {
        this.percentCredit = percentCredit;
    }

    public BigDecimal getPercentagePenalties() {
        return percentagePenalties;
    }

    public void setPercentagePenalties(BigDecimal percentagePenalties) {
        this.percentagePenalties = percentagePenalties;
    }

    public boolean isActiveCard() {
        return isActiveCard;
    }

    public void setActiveCard(boolean activeCard) {
        isActiveCard = activeCard;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyyy");
        String date = "";
        if (isActiveCredit)
            date = ", \n дата окончания кредитного периода " + formatDate.format(dateFinishCreditPeriod);
        return "Номер кредитной карты " + numberCard + ", срок действия карты " + periodCard +
                date + ", \n сумма кредитования " + amountCredit.toString() + currency +
                ", \n максимальная сумма кредитования " + limitCredit.toString() + currency +
                ", \n процент кредитования " + percentCredit.toString() +
                "%, \n пени " + percentagePenalties.toString() + "%";
    }


}
