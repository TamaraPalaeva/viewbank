package ru.sberbank.model.entity.credit;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.serviceEntity.Currency;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@ToString
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "request_credits")
public class RequestCredit {

    @Id
    @Column(name = "id_request")
    private String idrequest;

    private boolean workedOut = false;

    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Column(name = "limit_credit_card")
    private BigDecimal limitCreditCard;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id")
    private Client client;

    public RequestCredit() {
    }

    public RequestCredit(Currency currency, BigDecimal limitCreditCard, Client client) {
        this.idrequest = UUID.randomUUID().toString();
        this.currency = currency;
        this.limitCreditCard = limitCreditCard;
        this.client = client;
    }

    public RequestCredit(String idrequest, boolean workedOut, Currency currency, BigDecimal limitCreditCard, Client client) {
        this.idrequest = idrequest;
        this.workedOut = workedOut;
        this.currency = currency;
        this.limitCreditCard = limitCreditCard;
        this.client = client;
    }

    public String getIdrequest() {
        return idrequest;
    }

    public boolean isWorkedOut() {
        return workedOut;
    }

    public void setWorkedOut(boolean workedOut) {
        this.workedOut = workedOut;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public BigDecimal getLimitCreditCard() {
        return limitCreditCard;
    }

    public void setLimitCreditCard(BigDecimal limit) {
        this.limitCreditCard = limit;
    }


    @Override
    public String toString() {
        return "RequestCredit{" +
                "idrequest='" + idrequest + '\'' +
                ", workedOut=" + workedOut +
                ", currency=" + currency +
                ", limit=" + limitCreditCard +
                '}';
    }
}
