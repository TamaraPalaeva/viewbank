package ru.sberbank.model.entity.deposit;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@ToString
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "banking_accounts")
public class BankingAccount {
    @Id
    private String idBankingAccount;
    @OneToOne
    @JoinColumn(name = "number_bank_account")
    private NumberAccount numberBankAccount;
    @Column(name = "balance")
    private BigDecimal balance;
    private boolean isClose;
    @Column(name = "min_amount")
    private BigDecimal minAmount;
    @Column(name = "open_amount")
    private BigDecimal openAmount;
    @Column(name = "date_end")
    private Date dateEnd;
    @ManyToOne
    @JoinColumn(name = "rateOnDeposit_id")
    private RateOnDeposit rateOnDeposit;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id")
    private Client client;

    public BankingAccount() {
    }

    public BankingAccount(NumberAccount numberBankAccount, BigDecimal summAdd,
                          Date dateEnd, RateOnDeposit rateOnDeposit, Client client) {
        this.idBankingAccount = UUID.randomUUID().toString();
        this.numberBankAccount = numberBankAccount;
        this.setBalance(summAdd);
        this.dateEnd = dateEnd;
        this.rateOnDeposit = rateOnDeposit;
        this.client = client;
        this.openAmount = summAdd;
        this.setMinAmount(summAdd);
        this.isClose = false;
    }

    public NumberAccount getNumberBankAccount() {
        return numberBankAccount;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public RateOnDeposit getRateOnDeposit() {
        return rateOnDeposit;
    }

    public void setRateOnDeposit(RateOnDeposit rateOnDeposit) {
        this.rateOnDeposit = rateOnDeposit;
    }

    public BigDecimal getOpenAmount() {
        return openAmount;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public boolean isClose() {
        return isClose;
    }

    public void setClose(boolean close) {
        isClose = close;
    }

    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        return "Банковский счет " + numberBankAccount +
                ", баланс " + balance +
                ", срок вклада "+ format.format(dateEnd) +
                ", условия вклада " + rateOnDeposit;
    }
}
