package ru.sberbank.model.entity.deposit;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sberbank.model.entity.serviceEntity.DurationOfDeposit;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.UUID;

@ToString
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "rates_on_deposit")
public class RateOnDeposit {

    @Id
    @Column(name = "id_rate")
    private String idRate;
    @Column(name = "max_amount")
    private BigDecimal maxAmount;
    @Column(name = "min_amount")
    private BigDecimal minAmount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "duration_of_deposit")
    private DurationOfDeposit durationOfDeposit;

    @Column(name = "percent_deposit")
    private BigDecimal percentDeposit;
    //@Column(name = "isRefill")
    private boolean isRefill;
    //@Column(name = "isClose")
    private boolean isClose;
    // @Column(name = "isCapital")
    private boolean isCapital;
    //@Column(name = "isCharge")
    private boolean isCharge;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "rateOnDeposit")
    private List<BankingAccount> bankingAccounts;

    public RateOnDeposit() {
        this.idRate = UUID.randomUUID().toString();
    }

    public RateOnDeposit(BigDecimal maxAmount, BigDecimal minAmount, DurationOfDeposit durationOfDeposit,
                         BigDecimal percentDeposit, boolean isRefill, boolean isClose, boolean isCapital, boolean isCharge) {

        this.idRate = UUID.randomUUID().toString();
        this.maxAmount = maxAmount;
        this.minAmount = minAmount;
        this.durationOfDeposit = durationOfDeposit;
        this.percentDeposit = percentDeposit;
        this.isRefill = isRefill;
        this.isClose = isClose;
        this.isCapital = isCapital;
        this.isCharge = isCharge;
    }

    public String getIdrate() {
        return idRate;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public DurationOfDeposit getDurationOfDeposit() {
        return durationOfDeposit;
    }

    public void setDurationOfDeposit(DurationOfDeposit durationOfDeposit) {
        this.durationOfDeposit = durationOfDeposit;
    }

    public BigDecimal getPercentDeposit() {
        return percentDeposit;
    }

    public void setPercentDeposit(BigDecimal percentDeposit) {
        this.percentDeposit = percentDeposit;
    }

    public boolean isRefill() {
        return isRefill;
    }

    public void setRefill(boolean refill) {
        isRefill = refill;
    }

    public boolean isClose() {
        return isClose;
    }

    public void setClose(boolean close) {
        isClose = close;
    }

    public boolean isCapital() {
        return isCapital;
    }

    public void setCapital(boolean capital) {
        isCapital = capital;
    }

    public boolean isCharge() {
        return isCharge;
    }

    public void setCharge(boolean charge) {
        isCharge = charge;
    }

    @Override
    public String toString() {
        return "Вклад { ограничения суммы вклада " + minAmount.setScale(2, RoundingMode.CEILING) +
                " - " + maxAmount.setScale(2, RoundingMode.CEILING) +
                " RUB, длительность вклада " + durationOfDeposit.toString() +
                ", процент по вкладу " + percentDeposit.toString() +
                "%, возможность пополнения " + isRefill +
                ", возможность досрочного закрытия " + isClose +
                ", капитализация вклада " + isCapital +
                ", возможность снятия до закрытия вклада " + isCharge + '}';
    }
}
