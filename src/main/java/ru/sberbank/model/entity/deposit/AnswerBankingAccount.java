package ru.sberbank.model.entity.deposit;

import ru.sberbank.model.entity.person.Client;

import java.util.Date;

public class AnswerBankingAccount {
    private RateOnDeposit rateOnDeposit;
    private Client client;

    public AnswerBankingAccount(RateOnDeposit rateOnDeposit, Client client) {
        this.rateOnDeposit = rateOnDeposit;
        this.client = client;
    }

    public RateOnDeposit getRateOnDeposit() {
        return rateOnDeposit;
    }

    public void setRateOnDeposit(RateOnDeposit rateOnDeposit) {
        this.rateOnDeposit = rateOnDeposit;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
