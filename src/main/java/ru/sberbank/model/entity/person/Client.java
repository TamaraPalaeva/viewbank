package ru.sberbank.model.entity.person;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import ru.sberbank.model.entity.credit.CreditCard;
import ru.sberbank.model.entity.credit.RequestCredit;
import ru.sberbank.model.entity.deposit.BankingAccount;
import ru.sberbank.model.entity.serviceEntity.Authorization;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "clients")
public class Client implements Person {
    @Id
    @Column(name = "id_client")
    private String idClient;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "middle_name")
    private String middleName;

    @OneToOne()
    @JoinColumn(name = "passport_id")
    private Passport passport;

    @OneToOne(mappedBy = "client")
    private Authorization authorization;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "client")
    private List<RequestCredit> requestCredit;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "client")
    private List<CreditCard> creditCards;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "client")
    private List<BankingAccount> bankingAccounts;

    public Client() {
    }

    public Client(String idClient, String firstName, String lastName, String middleName, Passport passport) {
        this.idClient = idClient;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.passport = passport;
    }

    public Client(String firstName, String lastName, String middleName, Passport passport) {
        this.idClient = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.passport = passport;
    }

    public String getIdClient() {
        return idClient;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Passport getPassport() {
        return passport;
    }

    public void setPassport(Passport passport) {
        this.passport = passport;
    }

    public Authorization getAuthorization() {
        return authorization;
    }

    public void setAuthorization(Authorization authorization) {
        this.authorization = authorization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (idClient != null ? !idClient.equals(client.idClient) : client.idClient != null) return false;
        if (firstName != null ? !firstName.equals(client.firstName) : client.firstName != null) return false;
        if (lastName != null ? !lastName.equals(client.lastName) : client.lastName != null) return false;
        if (middleName != null ? !middleName.equals(client.middleName) : client.middleName != null) return false;
        return passport != null ? passport.equals(client.passport) : client.passport == null;
    }

    @Override
    public int hashCode() {
        int result = idClient != null ? idClient.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (passport != null ? passport.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + " " + middleName;
    }
}
