package ru.sberbank.model.entity.person;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.UUID;

@ToString
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "passports", uniqueConstraints =
@UniqueConstraint(columnNames = {"serial", "number"}))
public class Passport {
    @Id
    @Column(name = "id_passport")
    private String idPassport;
    @Column(name = "serial")
    private int serial;
    @Column(name = "number")
    private int number;
    @Column(name = "info")
    private String info;
    @OneToOne(mappedBy = "passport")
    private Client client;

    public Passport() {
    }

    public Passport(String idPassport, int serial, int number, String info) {
        this.idPassport = idPassport;
        this.serial = serial;
        this.number = number;
        this.info = info;
    }

    public Passport(int serial, int number, String info) {
        this.idPassport = UUID.randomUUID().toString();
        this.serial = serial;
        this.number = number;
        this.info = info;
    }

    public String getIdPassport() {
        return idPassport;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Passport passport = (Passport) o;

        if (serial != passport.serial) return false;
        if (number != passport.number) return false;
        if (idPassport != null ? !idPassport.equals(passport.idPassport) : passport.idPassport != null) return false;
        return info != null ? info.equals(passport.info) : passport.info == null;
    }

    @Override
    public int hashCode() {
        int result = idPassport != null ? idPassport.hashCode() : 0;
        result = 31 * result + serial;
        result = 31 * result + number;
        result = 31 * result + (info != null ? info.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return  " паспорт " +serial +  "-" + number +
                ", выдан " + info ;
    }
}
