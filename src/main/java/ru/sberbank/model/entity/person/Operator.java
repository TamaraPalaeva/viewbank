package ru.sberbank.model.entity.person;

public class Operator implements Person {
    private String firstName = "Хрюков";
    private String lastName = "Анатолий";
    private String middleName = "Владимирович";

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Override
    public String toString() {
        return  firstName + " " + lastName +  " " + middleName;
    }
}
