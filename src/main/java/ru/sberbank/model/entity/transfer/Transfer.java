package ru.sberbank.model.entity.transfer;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import ru.sberbank.model.entity.serviceEntity.Currency;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "transfers")
public class Transfer {
    @Id
    @Column(name = "id_transfer")
    private String idTransfer;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender")
    private NumberAccount sender;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipient")
    private NumberAccount recipient;

    @Column(name = "amount")
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private Currency currency = Currency.RUB;

    private boolean isFrod;
    private boolean isDone;

    public Transfer() {
    }

    public Transfer(NumberAccount sender, NumberAccount recipient, BigDecimal amount) {
        this.idTransfer = UUID.randomUUID().toString();
        this.sender = sender;
        this.recipient = recipient;
        this.amount = amount;
    }

    public String getIdTransfer() {
        return idTransfer;
    }

    public NumberAccount getSender() {
        return sender;
    }

    public void setSender(NumberAccount sender) {
        this.sender = sender;
    }

    public NumberAccount getRecipient() {
        return recipient;
    }

    public void setRecipient(NumberAccount recipient) {
        this.recipient = recipient;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public boolean isFrod() {
        return isFrod;
    }

    public void setFrod(boolean frod) {
        isFrod = frod;
    }

    public boolean isIsDone() {
        return isDone;
    }

    public void setIsDone(boolean ifDone) {
        this.isDone = ifDone;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return isFrod == transfer.isFrod &&
                isDone == transfer.isDone &&
                Objects.equals(idTransfer, transfer.idTransfer) &&
                Objects.equals(sender, transfer.sender) &&
                Objects.equals(recipient, transfer.recipient) &&
                Objects.equals(amount, transfer.amount) &&
                currency == transfer.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTransfer, sender, recipient, amount, currency, isFrod, isDone);
    }

    @Override
    public String toString() {
        return "Перевод: отправитель " + sender +
                ", получатель " + recipient +
                ", сумма перевода " + amount + " " + currency.toString();
    }
}
