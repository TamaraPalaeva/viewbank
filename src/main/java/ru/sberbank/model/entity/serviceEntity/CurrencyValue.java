package ru.sberbank.model.entity.serviceEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "currency_values")
public class CurrencyValue {
    @Id
    @Column(name = "id_currency")
    @Enumerated(EnumType.STRING)
    private Currency name;

    @Column(name = "value")
    private BigDecimal value;

    @Column(name = "value_one_rub")
    private BigDecimal valueOneRUB;

    public CurrencyValue() {
    }

    public CurrencyValue(Currency name, BigDecimal value, BigDecimal valueOneRUB) {
        this.name = name;
        this.value = value;
        this.valueOneRUB = valueOneRUB;
    }

    public Currency getName() {
        return name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValueOneRUB() {
        return valueOneRUB;
    }

    public void setValueOneRUB(BigDecimal valueOneRUB) {
        this.valueOneRUB = valueOneRUB;
    }

    @Override
    public String toString() {
        return  name.toString() + '=' + value.toString();
    }

}
