package ru.sberbank.model.entity.serviceEntity;


public enum Currency {
    RUB, EUR, USD;

    Currency() {
    }
}
