package ru.sberbank.model.entity.serviceEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sberbank.model.entity.deposit.RateOnDeposit;

import javax.persistence.*;
import java.util.List;

@ToString
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "duration_of_deposit")
public class DurationOfDeposit {

    @Id
    private String id;

    @Enumerated(EnumType.STRING)
    @Column(name = "name_duration")
    private NamePeriod nameDuration;
    @Column(name = "period")
    private int period;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER, mappedBy = "durationOfDeposit")
    private List<RateOnDeposit> rateOnDeposit;

    public DurationOfDeposit() {
    }

    public DurationOfDeposit(String id, NamePeriod nameDuration, int period) {
        this.id = id;
        this.nameDuration = nameDuration;
        this.period = period;
    }

    public NamePeriod getNameDuration() {
        return nameDuration;
    }

    public void setNameDuration(NamePeriod nameDuration) {
        this.nameDuration = nameDuration;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return id + " (" + period + " " + nameDuration + ")";
    }
}
