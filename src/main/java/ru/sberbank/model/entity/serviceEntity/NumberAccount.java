package ru.sberbank.model.entity.serviceEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sberbank.model.entity.credit.CreditCard;
import ru.sberbank.model.entity.deposit.BankingAccount;
import ru.sberbank.model.entity.transfer.Transfer;

import javax.persistence.*;
import java.util.List;

@ToString
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "number_accounts")
public class NumberAccount {

    @Id
    String number;

    @OneToOne(mappedBy = "numberCard")
    private CreditCard creditCard;

    @OneToOne(mappedBy = "numberBankAccount")
    private BankingAccount bankingAccount;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "recipient")
    private List<Transfer> recipients;
    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "sender")
    private List<Transfer> senders;

    public NumberAccount() {
    }

    public NumberAccount(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NumberAccount that = (NumberAccount) o;

        return number != null ? number.equals(that.number) : that.number == null;
    }

    @Override
    public int hashCode() {
        return number != null ? number.hashCode() : 0;
    }

    @Override
    public String toString() {
        return  number ;
    }
}
