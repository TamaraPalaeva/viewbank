package ru.sberbank.model.entity.serviceEntity;

public enum NamePeriod {
    MONTH, YEAR
}
