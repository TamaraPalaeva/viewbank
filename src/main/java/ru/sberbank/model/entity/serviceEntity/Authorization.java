package ru.sberbank.model.entity.serviceEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import ru.sberbank.model.entity.person.Client;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "authorization")
public class Authorization {
    @Id
    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;
    @OneToOne()
    @JoinColumn(name = "client_id", nullable = true)
    private Client client;

    public Authorization() {
    }

    public Authorization(String login, String password, Client client) {
        this.login = login;
        this.password = password;
        this.client = client;
    }

    public Authorization(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPerson(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return "Authorization{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", client=" + client +
                '}';
    }
}
