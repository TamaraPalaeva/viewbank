package ru.sberbank.model.service.dbService.credit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.AnswerCredit;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.repository.creditRepository.AnswerCreditRepository;
import ru.sberbank.model.service.dbService.service.ServiceEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerCreditRepositoryService implements ServiceEntityRepository<AnswerCredit> {

    @Autowired
    private AnswerCreditRepository answerCreditRepository;

    public void save(AnswerCredit answerCredit) {
        answerCreditRepository.save(answerCredit);
    }

    public void delete(AnswerCredit answerCredit) {
        answerCreditRepository.delete(answerCredit);
    }

    public void update(AnswerCredit answerCredit) {
        answerCreditRepository.save(answerCredit);
    }

    public Optional<AnswerCredit> findById(String id) {
        return answerCreditRepository.findById(id);
    }

    public List<AnswerCredit> findAll() {
        return (List<AnswerCredit>) answerCreditRepository.findAll();
    }

    public List<AnswerCredit> findAllByClient(Client client) {
        return answerCreditRepository.findAllByClient(client);
    }

    public List<AnswerCredit> findByClientDontApproved(Client client) {
        return (List<AnswerCredit>) answerCreditRepository.findByClientAndApprovedFalse(client.getIdClient());
    }
}
