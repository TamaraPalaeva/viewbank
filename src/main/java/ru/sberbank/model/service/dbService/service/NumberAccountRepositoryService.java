package ru.sberbank.model.service.dbService.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;
import ru.sberbank.model.repository.serviceRepository.NumberAccountRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class NumberAccountRepositoryService implements ServiceEntityRepository<NumberAccount> {
    @Autowired
    private NumberAccountRepository numberAccountRepository;

    @Override
    public void save(NumberAccount numberAccount) {
        numberAccountRepository.save(numberAccount);
    }

    @Override
    public void delete(NumberAccount numberAccount) {
        numberAccountRepository.delete(numberAccount);
    }

    @Override
    public void update(NumberAccount numberAccount) {
        numberAccountRepository.save(numberAccount);
    }

    @Override
    public List<NumberAccount> findAll() {
        return (List<NumberAccount>) numberAccountRepository.findAll();
    }

    public NumberAccount findAllByNumber(String number) {
        return numberAccountRepository.findAllByNumber(number);
    }

    public List<NumberAccount> findAllByBankingAccount_ClientAndCreditCard_Client(Client client) {
        List<NumberAccount> numberAccounts = new ArrayList<>();
        List<String> list1 = (List<String>) numberAccountRepository.findNumberAccountsByBankingAccount_Client_IdClient(client.getIdClient());
        List<String> list2 = (List<String>) numberAccountRepository.findNumberAccountsByCreditCard_Client_IdClient(client.getIdClient());
        if (list1 != null) {
            for (String number : list1) {
                numberAccounts.add(findAllByNumber(number));
            }
        }
        if (list2 != null) {
            for (String number : list2) {
                numberAccounts.add(findAllByNumber(number));
            }
        }
        return numberAccounts;
    }
}
