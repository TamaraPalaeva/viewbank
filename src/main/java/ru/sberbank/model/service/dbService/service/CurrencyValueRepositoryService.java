package ru.sberbank.model.service.dbService.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.serviceEntity.CurrencyValue;
import ru.sberbank.model.repository.serviceRepository.CurrencyValueRepository;

import java.util.List;

@Service
public class CurrencyValueRepositoryService implements ServiceEntityRepository<CurrencyValue> {

    @Autowired
    private CurrencyValueRepository currencyValueRepository;

    @Override
    public void save(CurrencyValue currencyValue) {
        currencyValueRepository.save(currencyValue);
    }

    @Override
    public void delete(CurrencyValue currencyValue) {
        currencyValueRepository.delete(currencyValue);
    }

    @Override
    public void update(CurrencyValue currencyValue) {
        currencyValueRepository.save(currencyValue);
    }

    @Override
    public List<CurrencyValue> findAll() {
        return (List<CurrencyValue>) currencyValueRepository.findAll();
    }

    public CurrencyValue findByName(String name) {
        return currencyValueRepository.findByName(name);
    }


}
