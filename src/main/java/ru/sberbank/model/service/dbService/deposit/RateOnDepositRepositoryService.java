package ru.sberbank.model.service.dbService.deposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.deposit.RateOnDeposit;
import ru.sberbank.model.repository.depositRepository.RateOnDepositRepository;
import ru.sberbank.model.service.dbService.service.ServiceEntityRepository;

import java.util.List;

@Service
public class RateOnDepositRepositoryService implements ServiceEntityRepository<RateOnDeposit> {
    @Autowired
    private RateOnDepositRepository rateOnDepositRepository;

    @Override
    public void save(RateOnDeposit rateOnDeposit) {
        rateOnDepositRepository.save(rateOnDeposit);
    }

    @Override
    public void delete(RateOnDeposit rateOnDeposit) {
        rateOnDepositRepository.delete(rateOnDeposit);
    }

    @Override
    public void update(RateOnDeposit rateOnDeposit) {
        rateOnDepositRepository.save(rateOnDeposit);
    }

    @Override
    public List<RateOnDeposit> findAll() {
        return (List<RateOnDeposit>) rateOnDepositRepository.findAll();
    }
}
