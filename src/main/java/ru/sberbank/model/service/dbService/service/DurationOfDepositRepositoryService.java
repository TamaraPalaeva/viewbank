package ru.sberbank.model.service.dbService.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.serviceEntity.DurationOfDeposit;
import ru.sberbank.model.repository.serviceRepository.DurationOfDepositRepository;

import java.util.List;

@Service
public class DurationOfDepositRepositoryService implements ServiceEntityRepository<DurationOfDeposit> {

    @Autowired
    private DurationOfDepositRepository durationOfDepositRepository;

    @Override
    public void save(DurationOfDeposit durationOfDeposit) {
        durationOfDepositRepository.save(durationOfDeposit);
    }

    @Override
    public void delete(DurationOfDeposit durationOfDeposit) {
        durationOfDepositRepository.delete(durationOfDeposit);
    }

    @Override
    public void update(DurationOfDeposit durationOfDeposit) {
        durationOfDepositRepository.save(durationOfDeposit);
    }

    @Override
    public List<DurationOfDeposit> findAll() {
        return (List<DurationOfDeposit>) durationOfDepositRepository.findAll();
    }
}
