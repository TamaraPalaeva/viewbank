package ru.sberbank.model.service.dbService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.repository.entityRepository.ClientRepository;
import ru.sberbank.model.service.dbService.service.ServiceEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ClientRepositoryService implements ServiceEntityRepository<Client> {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public void save(Client client) {
        clientRepository.save(client);
    }

    @Override
    public void delete(Client client) {
        clientRepository.delete(client);
    }

    @Override
    public void update(Client client) {
        clientRepository.save(client);
    }

    @Override
    public List<Client> findAll() {
        return (List<Client>) clientRepository.findAll();
    }


    public Optional<Client> findById(String id) {
        return clientRepository.findById(id);
    }

    public Client findByPassport(int serial, int number) {
        return clientRepository.findByPassport(serial, number);
    }
}
