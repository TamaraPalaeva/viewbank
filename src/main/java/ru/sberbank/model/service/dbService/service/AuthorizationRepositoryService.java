package ru.sberbank.model.service.dbService.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.serviceEntity.Authorization;
import ru.sberbank.model.repository.serviceRepository.AutorizationRepository;

import java.util.List;

@Service
public class AuthorizationRepositoryService implements ServiceEntityRepository<Authorization> {

    @Autowired
    AutorizationRepository autorizationRepository;

    @Override
    public void save(Authorization authorization) {
        autorizationRepository.save(authorization);
    }

    @Override
    public void delete(Authorization authorization) {
        autorizationRepository.delete(authorization);
    }

    @Override
    public void update(Authorization authorization) {
        autorizationRepository.save(authorization);
    }

    @Override
    public List<Authorization> findAll() {
        return (List<Authorization>) autorizationRepository.findAll();
    }

    public Authorization findByLogin(String login) {
        return autorizationRepository.findByLogin(login);
    }
}
