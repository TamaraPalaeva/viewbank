package ru.sberbank.model.service.dbService.credit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.RequestCredit;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.repository.creditRepository.RequestCreditRepository;
import ru.sberbank.model.service.dbService.service.ServiceEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
public class RequestCreditRepositoryService implements ServiceEntityRepository<RequestCredit> {

    @Autowired
    RequestCreditRepository requestCreditRepository;

    @Override
    public void save(RequestCredit requestCredit) {
        requestCreditRepository.save(requestCredit);
    }

    @Override
    public void delete(RequestCredit requestCredit) {
        requestCreditRepository.delete(requestCredit);
    }

    @Override
    public void update(RequestCredit requestCredit) {
        requestCreditRepository.save(requestCredit);
    }

    public Optional<RequestCredit> findById(String id) {
        return requestCreditRepository.findById(id);
    }

    public List<RequestCredit> findAll() {
        return (List<RequestCredit>) requestCreditRepository.findAll();
    }

    public List<RequestCredit> findAllByClient(Client client) {
        return requestCreditRepository.findAllByClient(client);
    }

    public List<RequestCredit> findAllByWorkOut() {
        return (List<RequestCredit>) requestCreditRepository.findAllByWorkOut();
    }

}
