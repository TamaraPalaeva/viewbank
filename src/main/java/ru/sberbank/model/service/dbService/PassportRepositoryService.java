package ru.sberbank.model.service.dbService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.person.Passport;
import ru.sberbank.model.repository.entityRepository.PassportRepository;
import ru.sberbank.model.service.dbService.service.ServiceEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PassportRepositoryService implements ServiceEntityRepository<Passport> {

    @Autowired
    private PassportRepository passportRepository;

    public PassportRepositoryService() {
    }

    @Override
    public void save(Passport passport) {
        passportRepository.save(passport);
    }

    @Override
    public void delete(Passport passport) {
        passportRepository.delete(passport);
    }

    @Override
    public void update(Passport passport) {
        passportRepository.save(passport);
    }

    @Override
    public List<Passport> findAll() {
        return (List<Passport>) passportRepository.findAll();
    }

    public Optional<Passport> findById(String id) {
        return passportRepository.findById(id);
    }

    public Passport findBySerialAndNumber(int seral, int number) {
        return passportRepository.findBySerialAndNumber(seral, number);
    }
}
