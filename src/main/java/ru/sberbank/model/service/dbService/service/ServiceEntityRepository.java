package ru.sberbank.model.service.dbService.service;

import java.util.List;

public interface ServiceEntityRepository<T> {

    void save(T t);

    void delete(T t);

    void update(T t);

    List<T> findAll();
}
