package ru.sberbank.model.service.dbService.deposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.deposit.BankingAccount;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;
import ru.sberbank.model.repository.depositRepository.BankingAccountRepository;
import ru.sberbank.model.service.dbService.service.ServiceEntityRepository;

import java.util.Date;
import java.util.List;

@Service
public class BankingAccountRepositoryService implements ServiceEntityRepository<BankingAccount> {

    @Autowired
    private BankingAccountRepository bankingAccountRepository;

    @Override
    public void save(BankingAccount bankingAccount) {
        bankingAccountRepository.save(bankingAccount);
    }

    @Override
    public void delete(BankingAccount bankingAccount) {
        bankingAccountRepository.delete(bankingAccount);
    }

    @Override
    public void update(BankingAccount bankingAccount) {
        bankingAccountRepository.save(bankingAccount);
    }

    public BankingAccount findByNumber(NumberAccount numberBankingAccount) {
        return bankingAccountRepository.findByNumberBankAccount(numberBankingAccount.getNumber());
    }

    public List<BankingAccount> findAll() {
        return (List<BankingAccount>) bankingAccountRepository.findAll();
    }

    public List<BankingAccount> findAllByClient(Client client) {
        return bankingAccountRepository.findAllByClient(client.getIdClient());
    }

    public List<BankingAccount> findAllByClientNotClose(Client client) {
        return
                bankingAccountRepository.findAllByClientNotClose(client.getIdClient());
    }

    public List<BankingAccount> findAllByNotClose(Date date) {
        return bankingAccountRepository.findAllByNotCloseAndToDay(date);
    }
}
