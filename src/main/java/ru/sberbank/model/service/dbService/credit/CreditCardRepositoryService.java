package ru.sberbank.model.service.dbService.credit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.CreditCard;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;
import ru.sberbank.model.repository.creditRepository.CreditCardRepository;
import ru.sberbank.model.service.dbService.service.ServiceEntityRepository;

import java.util.Date;
import java.util.List;

@Service
public class CreditCardRepositoryService implements ServiceEntityRepository<CreditCard> {
    @Autowired
    private CreditCardRepository creditCardRepository;

    @Override
    public void save(CreditCard creditCard) {
        creditCardRepository.save(creditCard);
    }

    @Override
    public void delete(CreditCard creditCard) {
        creditCardRepository.delete(creditCard);
    }

    @Override
    public void update(CreditCard creditCard) {
        creditCardRepository.save(creditCard);
    }

    public CreditCard findByNumberCreditCard(NumberAccount numberCreditCard) {
        return creditCardRepository.findByNumberCard(numberCreditCard.getNumber());
    }

    public List<CreditCard> findAllByClient(Client client) {
        return creditCardRepository.findAllByClient(client);
    }

    public List<CreditCard> findAll() {
        return creditCardRepository.findAll();
    }

    public List<CreditCard> findAllByClientIsActiveCard(Client client) {
        return (List<CreditCard>) creditCardRepository.findAllByClientIsActiveCard(client.getIdClient());
    }

    public List<CreditCard> findAllByDateFinishCreditPeriod(Date date) {
        return (List<CreditCard>) creditCardRepository.findAllByDateFinishCreditPeriod(date);
    }
}