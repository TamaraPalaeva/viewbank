package ru.sberbank.model.service.dbService.transfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.person.Client;
import ru.sberbank.model.entity.transfer.Transfer;
import ru.sberbank.model.repository.transferRepository.TransferRepository;
import ru.sberbank.model.service.dbService.service.ServiceEntityRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TransferDAOService implements ServiceEntityRepository<Transfer> {
    @Autowired
    private TransferRepository transferRepository;

    @Override
    public void save(Transfer transfer) {
        transferRepository.save(transfer);
    }

    @Override
    public void delete(Transfer transfer) {
        transferRepository.delete(transfer);
    }

    @Override
    public void update(Transfer transfer) {
        transferRepository.save(transfer);
    }

    @Override
    public List<Transfer> findAll() {
        return (List<Transfer>) transferRepository.findAll();
    }

    public List<Transfer> findAllByIsFrod() {
        return transferRepository.findAllByISFrod();
    }

    public Set<Transfer> findAllByClient(Client client) {
        Set<Transfer> transfersClient = new HashSet<>();
        List<Transfer> transfersBankingAccount = (List<Transfer>) transferRepository.findFromBankingAccountByClient(client.getIdClient());
        List<Transfer> transfersCreditCard = (List<Transfer>) transferRepository.findFromCreditCardByClient(client.getIdClient());
        transfersClient.addAll(transfersBankingAccount);
        transfersBankingAccount.addAll(transfersCreditCard);
        return transfersClient;

    }
}

