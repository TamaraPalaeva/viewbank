package ru.sberbank.model.service.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.util.logging.Level;
import java.util.logging.Logger;

@Aspect
public class LoggingAspect {

    private static Logger LOG = Logger.getLogger(LoggingAspect.class.getName());

    @Around(value = "execution(* ru.sberbank.model.*(..))")
    public Object logMethod(ProceedingJoinPoint joinPoint) {
        LOG.log(Level.INFO, "Открытие операции..." + joinPoint.toString());
        Object retVal = null;
        try {
            retVal = joinPoint.proceed();
            LOG.log(Level.INFO, "Закрытие операции...." + joinPoint.toString());
        } catch (Throwable throwable) {
            LOG.log(Level.WARNING, "Операция не удалась. " + throwable.getMessage());
        }
        return retVal;
    }
}
