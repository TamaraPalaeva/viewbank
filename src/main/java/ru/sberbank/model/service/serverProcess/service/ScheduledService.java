package ru.sberbank.model.service.serverProcess.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.CreditCard;
import ru.sberbank.model.entity.deposit.BankingAccount;
import ru.sberbank.model.service.dbService.credit.CreditCardRepositoryService;
import ru.sberbank.model.service.dbService.deposit.BankingAccountRepositoryService;
import ru.sberbank.model.service.serverProcess.deposit.BankingAccountServiceImpl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class ScheduledService {
    private static final String CORN_DAY = "0 0 0 */1 * *";

    @Scheduled(cron = CORN_DAY)
    public void penaltiesPercents(@Autowired CreditCardRepositoryService repositoryService) {

        List<CreditCard> creditCards = repositoryService.findAllByDateFinishCreditPeriod(new Date());
        BigDecimal precent = new BigDecimal(0);
        BigDecimal summ = new BigDecimal(0);
        precent.setScale(2, RoundingMode.CEILING);
        summ.setScale(2, RoundingMode.CEILING);
        for (CreditCard creditCard : creditCards) {
            precent = creditCard.getPercentagePenalties();
            summ = creditCard.getAmountCredit();
            precent = precent.add(new BigDecimal(100));
            precent = summ.multiply(precent);
            precent = precent.divide(new BigDecimal(100));
            summ = summ.add(precent);
            creditCard.setAmountCredit(summ);
            repositoryService.update(creditCard);
        }
    }

    @Scheduled(cron = CORN_DAY)
    public void extensionBankingAccount(@Autowired BankingAccountServiceImpl bankingAccountService,
                                        @Autowired BankingAccountRepositoryService repositoryService) {
        List<BankingAccount> bankingAccounts = repositoryService.findAllByNotClose(new Date());
        for (BankingAccount bankAccount :
                bankingAccounts) {
            bankingAccountService.extensionBankingAccount(bankAccount);
        }
    }

}
