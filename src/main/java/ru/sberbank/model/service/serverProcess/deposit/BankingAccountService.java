package ru.sberbank.model.service.serverProcess.deposit;

import ru.sberbank.model.entity.deposit.AnswerBankingAccount;
import ru.sberbank.model.entity.deposit.BankingAccount;

import java.math.BigDecimal;

public interface BankingAccountService {
    void createBankingAccount(AnswerBankingAccount answerBankingAccount, BigDecimal summ);

    void creditBankingAccount(BankingAccount bankingAccount, BigDecimal summTransaction);

    void debitBankingAccount(BankingAccount bankingAccount, BigDecimal summTransaction);

    void extensionBankingAccount(BankingAccount bankingAccount);

    void capitalizeBankingAccount(BankingAccount bankingAccount);

    void closeBankingAccount(BankingAccount bankingAccount);
}
