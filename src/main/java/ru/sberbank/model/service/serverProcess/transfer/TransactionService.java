package ru.sberbank.model.service.serverProcess.transfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.CreditCard;
import ru.sberbank.model.entity.deposit.BankingAccount;
import ru.sberbank.model.entity.serviceEntity.Currency;
import ru.sberbank.model.entity.serviceEntity.CurrencyValue;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;
import ru.sberbank.model.entity.transfer.Transfer;
import ru.sberbank.model.exception.FindAccountException;
import ru.sberbank.model.service.dbService.credit.CreditCardRepositoryService;
import ru.sberbank.model.service.dbService.deposit.BankingAccountRepositoryService;
import ru.sberbank.model.service.dbService.service.CurrencyValueRepositoryService;
import ru.sberbank.model.service.dbService.service.NumberAccountRepositoryService;
import ru.sberbank.model.service.dbService.transfer.TransferDAOService;
import ru.sberbank.model.service.serverProcess.credit.CreditCardService;
import ru.sberbank.model.service.serverProcess.deposit.BankingAccountServiceImpl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionService {
    @Autowired
    private TransferDAOService transferDAOService;
    @Autowired
    private NumberAccountRepositoryService numberAccountRepositoryService;
    @Autowired
    private BankingAccountRepositoryService bankingAccountRepositoryService;
    @Autowired
    private CreditCardRepositoryService creditCardRepositoryService;
    @Autowired
    private BankingAccountServiceImpl accountService;
    @Autowired
    private CreditCardService cardService;
    @Autowired
    private CurrencyValueRepositoryService currencyValueRepositoryService;

    List<String> numberAccounts;
    private CurrencyValue currencySenderCreditCard;
    private Transfer transfer;

    public void transfer(Transfer transfer) throws FindAccountException {
        this.transfer = transfer;
        numberAccounts = listNumbers();
        if ((numberAccounts.contains(transfer.getSender().getNumber())
                && (numberAccounts.contains(transfer.getRecipient().getNumber())))) {
            transferInBank();
        } else if (!(numberAccounts.contains(transfer.getSender().getNumber())
                || !(numberAccounts.contains(transfer.getRecipient().getNumber())))) {
            outsiderParticipant();
        } else {
            throw new FindAccountException("Плательщик и получатель не являются клиентами банка");
        }
    }

    private void transferInBank() {
        transferSender(transfer.getSender(), transfer.getAmount());
        transferRecipient(transfer.getRecipient(), transfer.getAmount());
        transfer.setIsDone(true);
        transferDAOService.save(transfer);

    }

    private void transferSender(NumberAccount sender, BigDecimal amount) {
        if (sender.getNumber().length() == 20) {
            BankingAccount bankingAccount = bankingAccountRepositoryService.findByNumber(sender);
            accountService.creditBankingAccount(bankingAccount, amount);
        } else {
            CreditCard creditCard = creditCardRepositoryService.findByNumberCreditCard(sender);
            cardService.creditTransactionWithCreditCard(creditCard, amount);
            transfer.setCurrency(creditCard.getCurrency());
            currencySenderCreditCard = currencyValueRepositoryService.findByName(creditCard.getCurrency().toString());
        }
    }

    private void transferRecipient(NumberAccount recipient, BigDecimal amount) {
        amount = amountOnRUB(amount);
        if (recipient.getNumber().length() == 20) {
            BankingAccount bankingAccount = bankingAccountRepositoryService.findByNumber(recipient);
            accountService.debitBankingAccount(bankingAccount, amount);
        } else {
            CreditCard creditCard = creditCardRepositoryService.findByNumberCreditCard(recipient);
            cardService.debitTransactionWithCreditCard(creditCard, amount);
        }
    }

    private BigDecimal amountOnRUB(BigDecimal amount) {
        if (!(transfer.getCurrency().equals(Currency.RUB))) {
            amount = amount.multiply(currencySenderCreditCard.getValue());
            amount.setScale(2, RoundingMode.CEILING);
        }
        return amount;
    }

    private List<String> listNumbers() {
        List<NumberAccount> numberAccountList = numberAccountRepositoryService.findAll();
        List<String> list = new ArrayList<>();
        for (NumberAccount numberAccount : numberAccountList) {
            list.add(numberAccount.getNumber());
        }
        return list;
    }

    private void outsiderParticipant() {
        if (!(numberAccounts.contains(transfer.getSender().getNumber()))) {
            if (transfer.getRecipient().getNumber().length() == 20) {
                BankingAccount bankingAccount = bankingAccountRepositoryService.findByNumber(transfer.getRecipient());
                accountService.debitBankingAccount(bankingAccount, transfer.getAmount());
            } else {
                CreditCard creditCard = creditCardRepositoryService.findByNumberCreditCard(transfer.getRecipient());
                cardService.debitTransactionWithCreditCard(creditCard, transfer.getAmount());
            }
            transfer.setIsDone(true);
            transferDAOService.update(transfer);
        } else {
            transfer.setFrod(true);
            transferDAOService.update(transfer);
        }
    }

    public void transferIsFrod(Transfer transfer) {
        BigDecimal sumAndPercent = transfer.getAmount();
        sumAndPercent = sumAndPercent.multiply(new BigDecimal(1.05));

        transferSender(transfer.getSender(), sumAndPercent);

        transfer.setIsDone(true);
        transferDAOService.update(transfer);
    }


}
