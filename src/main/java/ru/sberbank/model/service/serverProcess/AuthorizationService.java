package ru.sberbank.model.service.serverProcess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.serviceEntity.Authorization;
import ru.sberbank.model.exception.AuthorizationException;
import ru.sberbank.model.service.dbService.service.AuthorizationRepositoryService;

@Service
public class AuthorizationService {
    @Autowired
    private AuthorizationRepositoryService authorizationRepositoryService;

    public boolean authorization(String login, String password) {
        Authorization authorization = authorizationRepositoryService.findByLogin(login);
        return (authorization.getPassword().equals(password));
    }

    public String openEntity(String login, String password) {
        if (authorization(login,password)) {
            return (authorizationRepositoryService.findByLogin(login).getClient() == null) ? "operator" : "client";
        } else throw new AuthorizationException("Неверны данные. Проверьте логин и пароль");
    }

}
