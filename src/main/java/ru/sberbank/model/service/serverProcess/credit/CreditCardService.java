package ru.sberbank.model.service.serverProcess.credit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.credit.AnswerCredit;
import ru.sberbank.model.entity.credit.CreditCard;
import ru.sberbank.model.entity.serviceEntity.Currency;
import ru.sberbank.model.entity.serviceEntity.CurrencyValue;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;
import ru.sberbank.model.exception.PeriodCreditException;
import ru.sberbank.model.exception.SummException;
import ru.sberbank.model.service.dbService.credit.CreditCardRepositoryService;
import ru.sberbank.model.service.dbService.service.CurrencyValueRepositoryService;
import ru.sberbank.model.service.dbService.service.NumberAccountRepositoryService;
import ru.sberbank.model.service.serverProcess.service.GenerateNumber;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
@Component
@Service
public class CreditCardService implements CreditServise {
    @Autowired
    CreditCardRepositoryService creditCardRepositoryService;
    @Autowired
    CurrencyValueRepositoryService currencyValueRepositoryService;
    @Autowired
    GenerateNumber generateNumber;
    @Autowired
    NumberAccountRepositoryService numberAccountRepositoryService;

    private CreditCard creditCard;

    @Override
    public void createCreditCard(AnswerCredit answerCredit) {
        NumberAccount numberCreditCard = generateNumber.generateNumberCreditCard();
        numberAccountRepositoryService.save(numberCreditCard);
        String periodActiveCard = generatePeriodCreditCard();
        creditCardRepositoryService.save(new CreditCard(numberCreditCard, periodActiveCard, new Date(),
                answerCredit.getLimitCreditCard(), new BigDecimal("0"), answerCredit.getCurrency(), answerCredit.getPercentCredit(),
                answerCredit.getPercentagePenalties(), answerCredit.getClient()));
    }

    private String generatePeriodCreditCard() {
        Calendar calendar = Calendar.getInstance();
        String period = calendar.get(Calendar.MONTH) + "/" + (calendar.get(Calendar.YEAR) + 3);
        return period;
    }

    @Override
    public void creditTransactionWithCreditCard(CreditCard creditCard, BigDecimal sumTransaction) {
        this.creditCard = creditCard;
        if (creditCard.isActiveCredit()) {
            if (creditCard.getDateFinishCreditPeriod().getTime() > new Date().getTime()) {
                transactionCredit(sumTransaction);
            } else throw new PeriodCreditException("Период кредитования окончен. Закройте кредит.");
        } else {
            transactionCredit(sumTransaction);
            creditCard.setActiveCredit(true);
            creditCard.setDateFinishCreditPeriod(new Date(new Date().getTime() + 2592000000L));
            creditCardRepositoryService.update(creditCard);
        }
    }

    private void transactionCredit(BigDecimal sumTransaction) {
        BigDecimal commission = commissionTransaction(sumTransaction);
        sumTransaction = sumTransaction.add(commission);
        if (sumTransaction.compareTo(differenceCredit(creditCard)) <= 0) {
            BigDecimal amountCredit = creditCard.getAmountCredit();
            amountCredit = amountCredit.add(sumTransaction);
            creditCard.setAmountCredit(amountCredit);
            creditCardRepositoryService.update(creditCard);
        } else throw new SummException("Сумма кредита с комиссией превышает лимит.");
    }

    public BigDecimal commissionTransaction(BigDecimal sumTransaction) {
        sumTransaction.setScale(2, BigDecimal.ROUND_CEILING);
        BigDecimal commission = new BigDecimal("0");
        commission.setScale(2, BigDecimal.ROUND_CEILING);
        if (creditCard.getCurrency().toString().equals("RUB")) {
            commission = sumTransaction.multiply(creditCard.getPercentCredit());
            commission = commission.divide(new BigDecimal(100));
        } else {
            BigDecimal percent = creditCard.getPercentCredit().add(new BigDecimal(10));
            percent.setScale(2, BigDecimal.ROUND_CEILING);
            percent = percent.divide(new BigDecimal(100));
            commission = sumTransaction.multiply(percent);
        }
        return commission;
    }

    private BigDecimal differenceCredit(CreditCard creditCard) {
        BigDecimal sumCredit = creditCard.getAmountCredit();
        BigDecimal difference = creditCard.getLimitCredit();
        sumCredit.setScale(2, BigDecimal.ROUND_CEILING);
        difference.setScale(2, BigDecimal.ROUND_CEILING);
        difference = difference.subtract(sumCredit);
        return difference;
    }

    @Override
    public void debitTransactionWithCreditCard(CreditCard creditCard, BigDecimal sumTransaction) {
        if (!(creditCard.getAmountCredit().compareTo(new BigDecimal(0)) == 0)) {
            BigDecimal amountCredit = creditCard.getAmountCredit();
            if (creditCard.getCurrency().equals(Currency.RUB)) {
                amountCredit = amountCredit.subtract(sumTransaction);
                notActiveCredit(creditCard);
                creditCard.setAmountCredit(amountCredit);
                creditCardRepositoryService.update(creditCard);
            } else {
                CurrencyValue currencyValue = currencyValueRepositoryService.findByName(creditCard.getCurrency().toString());
                sumTransaction = sumTransaction.multiply(currencyValue.getValueOneRUB());
                sumTransaction = sumTransaction.multiply(new BigDecimal(110));
                sumTransaction = sumTransaction.divide(new BigDecimal(100));
                amountCredit = amountCredit.subtract(sumTransaction);
                notActiveCredit(creditCard);
                creditCard.setAmountCredit(amountCredit);
                creditCardRepositoryService.update(creditCard);
            }
        }

    }

    private void notActiveCredit(CreditCard creditCard) {
        if (creditCard.getAmountCredit().compareTo(new BigDecimal(0)) == 0) {
            creditCard.setActiveCredit(false);
            creditCardRepositoryService.update(creditCard);
        }
    }

    @Override
    public void closeCreditCard(CreditCard creditCard) {
        String[] periodCardString = creditCard.getPeriodCard().split("/");
        int[] periodCard = new int[2];
        periodCard[0] = Integer.parseInt(periodCardString[0]);
        periodCard[1] = Integer.parseInt(periodCardString[1]);

        Calendar today = Calendar.getInstance();
        today.setTime(new Date());

        if (((today.get(Calendar.MONTH) == periodCard[0]) && (today.get(Calendar.YEAR) == periodCard[1]))
                && (creditCard.getAmountCredit().compareTo(new BigDecimal(0)) == 0)){
            creditCard.setActiveCard(false);
        }
    }

}
