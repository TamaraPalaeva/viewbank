package ru.sberbank.model.service.serverProcess.service;

import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;

import java.util.Random;

@Service
public  class GenerateNumber {

    public GenerateNumber() {
    }

    public NumberAccount generateNumberCreditCard() {
        String numberCard = "427654";
        long numberRandom = 0;
        while ((numberRandom <= 0)) {
            numberRandom = new Random().nextInt();
        }
        if (String.valueOf(numberRandom).length() < 10) {
            int col = 10 - String.valueOf(numberRandom).length();
            for (int i = 0; i < col; i++) {
                numberRandom = numberRandom * 10;
            }
            numberCard += numberRandom;
        } else numberCard += String.valueOf(numberRandom).substring(0, 10);
        NumberAccount numberAccount = new NumberAccount(numberCard);
        return numberAccount;
    }
    public NumberAccount generateNumberBankingAccount() {
        String numberCard = "40817810554";
        long numberRandom = 0;
        while ((numberRandom <= 0)) {
            numberRandom = new Random().nextInt();
        }
        if (String.valueOf(numberRandom).length() < 9) {
            int col = 10 - String.valueOf(numberRandom).length();
            for (int i = 0; i < col; i++) {
                numberRandom = numberRandom * 10;
            }
            numberCard += numberRandom;
        } else numberCard += String.valueOf(numberRandom).substring(0, 9);
        NumberAccount numberAccount = new NumberAccount(numberCard);
        return numberAccount;
    }
}
