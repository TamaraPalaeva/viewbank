package ru.sberbank.model.service.serverProcess.credit;

import ru.sberbank.model.entity.credit.AnswerCredit;
import ru.sberbank.model.entity.credit.CreditCard;

import java.math.BigDecimal;

public interface CreditServise {
    void createCreditCard(AnswerCredit answerCredit);
    void creditTransactionWithCreditCard(CreditCard creditCard, BigDecimal sumTransaction);
    void debitTransactionWithCreditCard(CreditCard creditCard, BigDecimal sumTransaction);
    void closeCreditCard(CreditCard creditCard);
}
