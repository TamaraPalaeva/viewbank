package ru.sberbank.model.service.serverProcess.deposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.deposit.AnswerBankingAccount;
import ru.sberbank.model.entity.deposit.BankingAccount;
import ru.sberbank.model.entity.serviceEntity.DurationOfDeposit;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;
import ru.sberbank.model.exception.SummException;
import ru.sberbank.model.service.dbService.deposit.BankingAccountRepositoryService;
import ru.sberbank.model.service.dbService.service.NumberAccountRepositoryService;
import ru.sberbank.model.service.serverProcess.service.GenerateNumber;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Service
public class BankingAccountServiceImpl implements BankingAccountService {
    @Autowired
    GenerateNumber generateNumber;
    @Autowired
    BankingAccountRepositoryService bankingAccountRepositoryService;
    @Autowired
    NumberAccountRepositoryService numberAccountRepositoryService;

    @Override
    public void createBankingAccount(AnswerBankingAccount answerBankingAccount, BigDecimal simmAdd) {
        if ((simmAdd.compareTo(answerBankingAccount.getRateOnDeposit().getMinAmount()) >= 0)
                && (simmAdd.compareTo(answerBankingAccount.getRateOnDeposit().getMaxAmount()) < 0)) {
            NumberAccount numberBankingAccount = generateNumber.generateNumberBankingAccount();
            numberAccountRepositoryService.save(numberBankingAccount);
            Date dateEnd = addDate(new Date(), answerBankingAccount.getRateOnDeposit().getDurationOfDeposit());
            bankingAccountRepositoryService.save(new BankingAccount(numberBankingAccount, simmAdd, dateEnd,
                    answerBankingAccount.getRateOnDeposit(), answerBankingAccount.getClient()));
        } else {
            throw new SummException("Сумма не входит в ограничения");
        }
    }

    @Override
    public void creditBankingAccount(BankingAccount bankingAccount, BigDecimal summTransaction) {
        BigDecimal reserveAmount = bankingAccount.getBalance().subtract(bankingAccount.getOpenAmount());
        if (bankingAccount.getRateOnDeposit().isCharge()) {
            if (reserveAmount.compareTo(summTransaction) >= 0) {
                bankingAccount.setBalance(bankingAccount.getBalance().subtract(summTransaction));
                if (bankingAccount.getMinAmount().compareTo(bankingAccount.getBalance()) == 1) {
                    bankingAccount.setMinAmount(bankingAccount.getMinAmount());
                }
                bankingAccountRepositoryService.update(bankingAccount);
            } else throw new SummException("Недостаточно средств на счету");
        } else throw new SummException("Счет заблокирован для снятия денежных средств");
    }

    @Override
    public void debitBankingAccount(BankingAccount bankingAccount, BigDecimal summTransaction) {
        BigDecimal reserveAmount = bankingAccount.getRateOnDeposit().getMaxAmount()
                .subtract(bankingAccount.getBalance());
        if (bankingAccount.getRateOnDeposit().isRefill()) {
            if (reserveAmount.compareTo(summTransaction) >= 0) {
                bankingAccount.setBalance(bankingAccount.getBalance().add(summTransaction));
                bankingAccountRepositoryService.update(bankingAccount);
            } else throw new SummException("Получаемая сумма превышает максимальную сумму по тарифу");
        } else throw new SummException("Вклад нельзя пополнять");

    }

    @Override
    public void extensionBankingAccount(BankingAccount bankingAccount) {
        if ((equaliseToday(bankingAccount)) && !(bankingAccount.isClose())) {
            bankingAccount.setDateEnd(addDate(bankingAccount.getDateEnd(), bankingAccount.getRateOnDeposit().getDurationOfDeposit())); }
        bankingAccountRepositoryService.update(bankingAccount);


}

    private Date addDate(Date date, DurationOfDeposit durationOfDeposit) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (durationOfDeposit.getNameDuration().toString().equals("MONTH")) {
            calendar.add(Calendar.MONTH, durationOfDeposit.getPeriod());
        } else if (durationOfDeposit.getNameDuration().toString().equals("YEAR")) {
            calendar.add(Calendar.YEAR, durationOfDeposit.getPeriod());
        }
        return  calendar.getTime();
    }

    @Override
    public void capitalizeBankingAccount(BankingAccount bankingAccount) {
        BigDecimal percent = new BigDecimal(10);
        percent = percent.divide(new BigDecimal(12), 2, RoundingMode.CEILING);
        BigDecimal amountCapit;
        if (bankingAccount.getRateOnDeposit().isCapital()) {
            amountCapit = bankingAccount.getMinAmount();
        } else {
            amountCapit = bankingAccount.getOpenAmount();
        }
        amountCapit = amountCapit.multiply(percent);
        bankingAccount.setBalance(bankingAccount.getBalance().add(amountCapit));
        bankingAccountRepositoryService.update(bankingAccount);
    }

    @Override
    public void closeBankingAccount(BankingAccount bankingAccount) {

        if (bankingAccount.getRateOnDeposit().isClose()) {
            bankingAccount.setClose(true);
            bankingAccount.setBalance(new BigDecimal(0));
        } else if (equaliseToday(bankingAccount)) {
            bankingAccount.setClose(true);
            bankingAccount.setBalance(new BigDecimal(0));
        }
        bankingAccountRepositoryService.update(bankingAccount);

    }

    private boolean equaliseToday(BankingAccount bankingAccount) {
        Calendar today = new GregorianCalendar();
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.setTime(bankingAccount.getDateEnd());
        if (today.get(Calendar.YEAR) == dayEnd.get(Calendar.YEAR) &&
                today.get(Calendar.DAY_OF_YEAR) == dayEnd.get(Calendar.DAY_OF_YEAR)) {
            return true;
        }
        return false;
    }
}
