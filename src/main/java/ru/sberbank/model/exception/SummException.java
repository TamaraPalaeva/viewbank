package ru.sberbank.model.exception;

public class SummException extends RuntimeException {
    public SummException(String message) {
        super(message);
    }
}
