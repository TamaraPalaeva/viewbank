package ru.sberbank.model.exception;

public class FindAccountException extends Throwable {
    public FindAccountException(String message) {
        super(message);
    }
}
