package ru.sberbank.model.exception;

public class PeriodCreditException extends RuntimeException {
    public PeriodCreditException(String message) {super(message);
    }
}
