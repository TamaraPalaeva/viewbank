package ru.sberbank.model.repository.transferRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.transfer.Transfer;

import java.util.List;

@Repository
public interface TransferRepository extends CrudRepository<Transfer, String> {

    @Query("select t from Transfer t inner join BankingAccount b where (t.recipient.number = b.numberBankAccount.number or t.sender.number = b.numberBankAccount.number) and b.client.idClient = ?1")
    Iterable<Transfer> findFromBankingAccountByClient(String client);

    @Query("select t from Transfer t inner join CreditCard c where (t.recipient.number = c.numberCard.number or t.sender.number = c.numberCard.number) and c.client.idClient = ?1")
    Iterable<Transfer> findFromCreditCardByClient(String client);

    @Query("select t from Transfer t where t.isFrod = true and t.isDone = false ")
    List<Transfer> findAllByISFrod();

}
