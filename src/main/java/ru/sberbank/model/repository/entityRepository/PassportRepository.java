package ru.sberbank.model.repository.entityRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.person.Passport;

@Repository
public interface PassportRepository extends CrudRepository<Passport, String> {

    @Query("select p from Passport p where p.serial = :serial and p.number = :number")
    Passport findBySerialAndNumber(@Param("serial") int serial, @Param("number") int number);

}
