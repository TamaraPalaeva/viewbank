package ru.sberbank.model.repository.entityRepository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.person.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client, String> {
    @Query("select c from Client c WHERE c.passport.serial = :serial and c.passport.number = :number")
    Client findByPassport(@Param("serial") int serial, @Param("number") int number);
}
