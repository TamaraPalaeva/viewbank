package ru.sberbank.model.repository.creditRepository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.credit.AnswerCredit;
import ru.sberbank.model.entity.person.Client;

import java.util.List;

@Repository
public interface AnswerCreditRepository extends CrudRepository<AnswerCredit, String> {

    @Query("select a from AnswerCredit a where a.client = ?1")
    List<AnswerCredit> findAllByClient(Client client);

    @Query("select a from AnswerCredit a where a.client.idClient = :client and a.approved = false")
    Iterable<AnswerCredit> findByClientAndApprovedFalse(@Param("client") String client);
}
