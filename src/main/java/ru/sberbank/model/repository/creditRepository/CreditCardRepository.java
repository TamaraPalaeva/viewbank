package ru.sberbank.model.repository.creditRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.credit.CreditCard;
import ru.sberbank.model.entity.person.Client;

import java.util.Date;
import java.util.List;


@Repository
public interface CreditCardRepository extends CrudRepository<CreditCard, String> {

    @Query("select c from CreditCard c")
    List<CreditCard> findAll();

    @Query("select c from CreditCard c where c.numberCard.number = ?1")
    CreditCard findByNumberCard(String numberCard);

    @Query("select c from CreditCard c where c.client = ?1")
    List<CreditCard> findAllByClient(Client client);

    @Query("select c from CreditCard c where c.client.idClient = ?1 and c.isActiveCard = true")
    Iterable<CreditCard> findAllByClientIsActiveCard(String client);

    @Query("select c from CreditCard c where c.dateFinishCreditPeriod < :date")
    Iterable<CreditCard> findAllByDateFinishCreditPeriod(@Param("date") Date dateToDay);
}
