package ru.sberbank.model.repository.creditRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.credit.RequestCredit;
import ru.sberbank.model.entity.person.Client;

import java.util.List;

@Repository
public interface RequestCreditRepository extends CrudRepository<RequestCredit, String> {
    @Query("select r from RequestCredit r where r.client = :client")
    List<RequestCredit> findAllByClient(@Param("client") Client client);

    @Query("select r from RequestCredit r where r.workedOut = false")
    Iterable<RequestCredit> findAllByWorkOut();
}
