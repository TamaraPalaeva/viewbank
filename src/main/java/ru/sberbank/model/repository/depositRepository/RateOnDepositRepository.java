package ru.sberbank.model.repository.depositRepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.deposit.RateOnDeposit;

@Repository
public interface RateOnDepositRepository extends CrudRepository<RateOnDeposit, String> {

}
