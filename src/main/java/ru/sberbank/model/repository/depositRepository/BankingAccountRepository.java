package ru.sberbank.model.repository.depositRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.deposit.BankingAccount;

import java.util.Date;
import java.util.List;


@Repository
public interface BankingAccountRepository extends CrudRepository<BankingAccount, String> {

    @Query("select  b from BankingAccount b where  b.numberBankAccount.number = ?1")
    BankingAccount findByNumberBankAccount(String numberBankAccount);

    @Query("select  b from BankingAccount b where b.client.idClient = ?1")
    List<BankingAccount> findAllByClient(String client);

    @Query("select  b from BankingAccount b where b.client = ?1 and  b.isClose = false ")
    List<BankingAccount> findAllByClientNotClose(String client);

    @Query("select  b from BankingAccount b where b.dateEnd = ?1 and  b.isClose = false ")
    List<BankingAccount> findAllByNotCloseAndToDay(Date date);
}
