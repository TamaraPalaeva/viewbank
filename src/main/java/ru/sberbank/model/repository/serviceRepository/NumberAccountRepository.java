package ru.sberbank.model.repository.serviceRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;

@Repository
public interface NumberAccountRepository extends CrudRepository<NumberAccount, String> {

    @Query("select n from  NumberAccount n where n.number = :num")
    NumberAccount findAllByNumber(@Param("num") String num);


    @Query("select b.numberBankAccount.number from BankingAccount b where b.client.idClient = ?1")
    Iterable<String> findNumberAccountsByBankingAccount_Client_IdClient(String bankingAccount_client);

    @Query("select c.numberCard.number from CreditCard c where c.client.idClient = ?1")
    Iterable<String> findNumberAccountsByCreditCard_Client_IdClient(String creditCard_client);
}
