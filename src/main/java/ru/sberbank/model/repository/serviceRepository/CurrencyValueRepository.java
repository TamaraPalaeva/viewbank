package ru.sberbank.model.repository.serviceRepository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.serviceEntity.CurrencyValue;

@Repository
public interface CurrencyValueRepository extends CrudRepository<CurrencyValue, String> {

    @Query("select c from CurrencyValue c where c.name = :name")
    CurrencyValue findByName(@Param("name") String name);

}
