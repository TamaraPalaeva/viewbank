package ru.sberbank.model.repository.serviceRepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.serviceEntity.DurationOfDeposit;

@Repository
public interface DurationOfDepositRepository extends CrudRepository<DurationOfDeposit, String> {

}
