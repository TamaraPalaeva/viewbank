package ru.sberbank.model.repository.serviceRepository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sberbank.model.entity.serviceEntity.Authorization;

@Repository
public interface AutorizationRepository extends CrudRepository<Authorization, String> {

    Authorization findByLogin(String login);
}
