package ru.sberbank.model.service.serverProcess.service;

import org.junit.Assert;
import org.junit.Test;
import ru.sberbank.model.entity.serviceEntity.NumberAccount;

public class GenerateNumberTest {

    @Test
    public void testGenerateNumberCreditCard() {
        GenerateNumber generateNumber = new GenerateNumber();
        NumberAccount number = generateNumber.generateNumberCreditCard();
        Assert.assertEquals(16, number.toString().length());
    }

    @Test
    public void testGenerateNumberBankingAccount() {
        GenerateNumber generateNumber = new GenerateNumber();
        NumberAccount number = generateNumber.generateNumberBankingAccount();
        Assert.assertEquals(20, number.toString().length());
    }
}